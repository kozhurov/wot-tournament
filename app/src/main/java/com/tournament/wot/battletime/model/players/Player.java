package com.tournament.wot.battletime.model.players;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.tournament.wot.battletime.core.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@DatabaseTable(tableName = "player")
public final class Player extends GenericPlayer {

    private static final String PRIVATE_FIELDS_OBJECT = "private";
    private static final String PRIVATE_OBJECT_FRIENDS_FIELD = "friends";
    private static final String JSON_USER_NAME = "nickname";
    private static final String JSON_LAST_BATTLE_TIME = "last_battle_time";
    private static final String JSON_CLAN_ID = "clan_id";
    private static final String TIMESTAMP_KEY = "updated_at";

    private ArrayList<Friend> pFriendsList;
    private ArrayList<Long> pFriendsCollection;

    @DatabaseField(columnName = "clan_id")
    private long mClanId;

    @DatabaseField(columnName = "updated_at")
    private int mUpdateAt;

    @DatabaseField(columnName = "tank_array")
    private String mTankArray;

    @ForeignCollectionField
    private ForeignCollection<Friend> friendsCollection;

    public Player() {

    }

    public Player(JSONObject pJSONObject) throws JSONException {

        mName = pJSONObject.getString(JSON_USER_NAME);
        mLastBattleTime = pJSONObject.getInt(JSON_LAST_BATTLE_TIME);

        mUpdateAt = pJSONObject.getInt(TIMESTAMP_KEY);
        mClanId = pJSONObject.optLong(JSON_CLAN_ID, Constants.WRONG_ARGS);

        JSONObject privateFields = pJSONObject.getJSONObject(PRIVATE_FIELDS_OBJECT);
        JSONArray friendsArray = privateFields.getJSONArray(PRIVATE_OBJECT_FRIENDS_FIELD);

        int arraysSize = friendsArray.length();
        pFriendsList = new ArrayList<Friend>(arraysSize);

        for (int i = 0; i < arraysSize; i++) {
            pFriendsList.add(new Friend(friendsArray.getLong(i)));
        }

        pFriendsCollection = new ArrayList<Long>(arraysSize);

        for (int i = 0; i < arraysSize; i++) {
            pFriendsCollection.add(friendsArray.getLong(i));
        }
    }

    public List<Friend> getFriendsList() {
        return pFriendsList;
    }

    public Collection<Long> getFriendsIdCollection() {
        return pFriendsCollection;
    }

    public int getUpdateAt() {
        return mUpdateAt;
    }

    public void setUpdateAt(int pUpdateAt) {
        mUpdateAt = pUpdateAt;
    }

    public ForeignCollection<Friend> getFriendsCollection() {
        return friendsCollection;
    }

    public long getClanId() {
        return mClanId;
    }

    public void setId(long pPlayerId) {
        mId = pPlayerId;
    }

    public String getTankArray() {
        return mTankArray;
    }

    public void setTankArray(String mTankArray) {
        this.mTankArray = mTankArray;
    }

    public void extractExtraRateInfo(JSONObject pJSONObject) throws JSONException {
        mWinRate = pJSONObject.getInt(WIN_RATE);
        mDamageRate =  pJSONObject.getInt(DAMAGE_RATE);
    }
}
