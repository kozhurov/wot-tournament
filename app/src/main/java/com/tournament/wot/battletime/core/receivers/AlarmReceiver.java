package com.tournament.wot.battletime.core.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.tournament.wot.battletime.core.WTApplication;
import com.tournament.wot.battletime.model.alarm.AlarmModel;
import com.tournament.wot.battletime.ui.activitys.MainActivity;
import com.tournament.wot.battletime.utils.PushAndNotifyUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import static android.content.Context.ALARM_SERVICE;


public class AlarmReceiver extends BroadcastReceiver {

    private static Calendar calendar;

    static {
        calendar = Calendar.getInstance();
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.e("AR", "alarm receive");

        if (((WTApplication) context.getApplicationContext()).isActivityStarted()) {
            translateInActivity(context, intent.getExtras());
        } else {
            PushAndNotifyUtils.showNotification(context, intent.getExtras());
        }
    }

    private void translateInActivity(Context pContext, Bundle pBundle) {
        Intent mainActivityIntent = new Intent(MainActivity.class.getSimpleName());
        mainActivityIntent.putExtras(pBundle);
        LocalBroadcastManager.getInstance(pContext).sendBroadcast(mainActivityIntent);
    }

    public static void setAlarm(Context context, long timeToShow, long pEventId) {
        AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);

        Intent intent = new Intent(context, AlarmReceiver.class);

        Bundle bundle = new Bundle();
        bundle.putString(PushAndNotifyUtils.MESSAGE_KEY, PushAndNotifyUtils.ALARM_TEXT);
        bundle.putLong(PushAndNotifyUtils.EVENT_ID_KEY, pEventId);
        intent.putExtras(bundle);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        am.set(AlarmManager.RTC_WAKEUP, timeToShow, pendingIntent);
    }

    public static void setAlarm(Context context, long timeToShow, long pEventId, Collection<Integer> arrayList) {
        calendar.setTimeInMillis(timeToShow);
        if((calendar.getActualMaximum(Calendar.DAY_OF_MONTH) - calendar.get(Calendar.DAY_OF_MONTH)) > 7 ) {
            if (arrayList != null && arrayList.size() > 0) {
                for (Integer integer : arrayList) {

                    calendar.set(Calendar.DAY_OF_WEEK, integer + 1);
                    setAlarm(context, calendar.getTimeInMillis(), pEventId);
                }
            } else {
                setAlarm(context, timeToShow, pEventId);
            }
        }
    }

    public static void setAlarmNew(Context context, long timeToShow,long pEventId ,ArrayList<AlarmModel> alarmModels,  Collection<Integer> arrayList){
        calendar.setTimeInMillis(timeToShow);
        if (arrayList != null) {
            int i = 0;
            for (Integer integer : arrayList) {
                calendar.set(Calendar.DAY_OF_WEEK, integer + 1);
                setAlarmNew(context, calendar.getTimeInMillis(), pEventId, alarmModels.get(i).getmId());
                i++;
            }
        }
    }

    public static void setAlarmNew(Context context, long timeToShow, long pEventId, int requestId) {
        AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);

        Intent intent = new Intent(context, AlarmReceiver.class);

        Bundle bundle = new Bundle();
        bundle.putString(PushAndNotifyUtils.MESSAGE_KEY, PushAndNotifyUtils.ALARM_TEXT);
        bundle.putLong(PushAndNotifyUtils.EVENT_ID_KEY, pEventId);
        intent.putExtras(bundle);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestId, intent, 0);

        am.set(AlarmManager.RTC_WAKEUP, timeToShow, pendingIntent);
    }

    public static void deleteAlarm(Context context, int id) {
        AlarmManager alarmManager = (AlarmManager) context
                .getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.cancel(pendingIntent);
    }
}
