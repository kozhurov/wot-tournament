package com.tournament.wot.battletime.core.callbacks;

import com.tournament.wot.battletime.model.battle.BattleModel;
import com.tournament.wot.battletime.model.players.Friend;
import com.tournament.wot.battletime.model.players.Player;
import com.tournament.wot.battletime.model.tanks.BestTanksModel;

import java.util.ArrayList;
import java.util.List;

public interface NetCallbacks {

    public void taskCompleteWithError(String pErrorCode);

    public void authComplete(String pRedirectUrl);

    public void loadMyInfoCompeted(Player pPlayer);

    public void baseSyncComplete(List<Friend> pFriends, int pTimestamp, Player pPlayer);

    public void battleCreated(BattleModel pBattleModel);

    public void battleUpdated(BattleModel pBattleModel);

    public void incrementSyncComplete(int pTimestamp, List<BattleModel> pBattles, List<Friend> pClanMembers);

    public void loadBestTanks(ArrayList<BestTanksModel> pBestTanksModel);

    public void changeCommanderComplete(long pNewCommanderId, String pEventUUID);

    public void userAnswerAccepted(int pTimestamp, int pAnswerKey);

    public void reSendComplete();

    public void deleteEventComplete();
}
