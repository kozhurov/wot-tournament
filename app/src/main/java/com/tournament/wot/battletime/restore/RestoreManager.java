package com.tournament.wot.battletime.restore;

import android.content.Context;
import android.util.Log;

import com.j256.ormlite.stmt.QueryBuilder;
import com.tournament.wot.battletime.model.alarm.AlarmModel;
import com.tournament.wot.battletime.model.battle.BattleModel;
import com.tournament.wot.battletime.model.battle.BattleModel.EventType;
import com.tournament.wot.battletime.model.players.Friend;
import com.tournament.wot.battletime.model.players.Participant;
import com.tournament.wot.battletime.model.players.Player;
import com.tournament.wot.battletime.model.tanks.BestTanksModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public final class RestoreManager extends Observable {

    private final DatabaseHelper mDatabaseHelper;

    private Player mPlayer;

    public RestoreManager(Context pContext) {
        mDatabaseHelper = new DatabaseHelper(pContext);
    }

    public void saveMe(Player pPlayer) {
        mPlayer = pPlayer;

        mDatabaseHelper.getPlayerDao().createOrUpdate(pPlayer);
        for (Friend model : mPlayer.getFriendsList()) {
            mDatabaseHelper.getFriendDao().createOrUpdate(model);
        }
    }

    public Player loadMe() {

        if (mDatabaseHelper.getPlayerDao().queryForAll().size() > 0) {
            return mDatabaseHelper.getPlayerDao().queryForAll().get(0);
        } else {
            return null;
        }
    }

    public void saveFriends(List<Friend> pFriendList) {
        for (Friend friend : pFriendList) {
            // friend.setPlayer(mPlayer);
            mDatabaseHelper.getFriendDao().createOrUpdate(friend);
        }
    }

    public void saveParticipant(Participant pParticipant) {
        mDatabaseHelper.getParticipantDao().update(pParticipant);
    }

    public void saveClanMembers(List<Friend> pFriendList) {

        if (pFriendList == null || pFriendList.isEmpty()) {
            return;
        }

        for (Friend friend : pFriendList) {
            Friend tempFriend = mDatabaseHelper.getFriendDao().queryForId((int) friend.getId());
            if (tempFriend != null) {
                tempFriend.setClanMember(true);
                mDatabaseHelper.getFriendDao().update(tempFriend);
            } else {
                mDatabaseHelper.getFriendDao().create(friend);
            }
        }
    }

    public List<Friend> loadFriend() {
        return mDatabaseHelper.getFriendDao().queryForEq("is_friend", true);
    }

    public List<Friend> loadClanMembers() {
        return mDatabaseHelper.getFriendDao().queryForEq("is_clan", true);
    }

    public void saveBattleModels(List<BattleModel> pBattleModels) {
        for (BattleModel simpleBattle : pBattleModels) {
            saveBattleModel(simpleBattle, false);
        }
        setChanged();
        notifyObservers();
    }

    public BattleModel getBattleModel(long pId) {
        BattleModel simpleBattle = mDatabaseHelper.getBattleModelDao().queryForId((int) pId);
        simpleBattle.setParticipants(mDatabaseHelper.getParticipantDao().queryForEq("simple_battles_id", simpleBattle.getId()));
        return simpleBattle;
    }

    public void updateBattleModel(BattleModel pBattleModel){
        mDatabaseHelper.getBattleModelDao().update(pBattleModel);
    }

    public void saveBattleModel(BattleModel pBattleModel, boolean pIsNeedNotify) {

        List<BattleModel> tempList = mDatabaseHelper.getBattleModelDao().queryForEq("UUID", pBattleModel.getUUID());
        if (tempList != null && tempList.size() > 0) {
            pBattleModel.setId(tempList.get(0).getId());
            mDatabaseHelper.getParticipantDao().delete(mDatabaseHelper.getParticipantDao().queryForEq("simple_battles_id", tempList.get(0).getId()));
            mDatabaseHelper.getBattleModelDao().update(pBattleModel);

        } else {

            mDatabaseHelper.getBattleModelDao().createOrUpdate(pBattleModel);
        }

        for (Participant mode : pBattleModel.getParticipants()) {
            if (mode.getParticipantId() > 0) {
                mode.setBattleModel(pBattleModel);
                mDatabaseHelper.getParticipantDao().createOrUpdate(mode);
            }
        }

        if (pIsNeedNotify) {
            setChanged();
            notifyObservers();
        }
    }

    public void deleteBattleModel(BattleModel pBattleModel) {
        mDatabaseHelper.getBattleModelDao().delete(pBattleModel);
    }

    public void deleteBattleModel(String pUUID) {
        List<BattleModel> battleModels = mDatabaseHelper.getBattleModelDao().queryForEq("UUID", pUUID);
        mDatabaseHelper.getBattleModelDao().delete(battleModels);
        setChanged();
        notifyObservers();
    }

    public List<BestTanksModel> getBestTanks() {
        return mDatabaseHelper.getBestTankDao().queryForAll();
    }

    public void saveBestTanks(ArrayList<BestTanksModel> arrayList) {
        for (BestTanksModel bestTanksModel : arrayList) {
            mDatabaseHelper.getBestTankDao().createOrUpdate(bestTanksModel);
        }
    }

    public List<BattleModel> getPlatoonsBattle() {
        return getBattleGeneric(EventType.PLATOON.ordinal());
    }

    public List<BattleModel> getClansBattle() {
        return getBattleGeneric(EventType.CLAN.ordinal());
    }

    public List<BattleModel> getCompanyBattle() {
        return getBattleGeneric(EventType.COMPANY.ordinal());
    }

    public List<BattleModel> getTrainingBattles() {
        return getBattleGeneric(EventType.TRAINING.ordinal());
    }

    private List<BattleModel> getBattleGeneric(int pType) {
        List<BattleModel> battleModels;

        QueryBuilder<BattleModel, Integer> qb = mDatabaseHelper.getBattleModelDao().queryBuilder();
        try {
            qb.where().eq("event_type", pType);
            qb.orderBy("start_battle_date", false);
            battleModels = qb.query();
        } catch (SQLException e) {
            e.printStackTrace();
            battleModels = new ArrayList<BattleModel>();
        }

        return battleModels;
    }

    public List<AlarmModel> getAlarmsByUUID(String UUID){
        QueryBuilder<AlarmModel, Integer> queryBuilder = mDatabaseHelper.getAlarmModels().queryBuilder();
        List<AlarmModel> alarmModels = null;
        try {
            alarmModels = queryBuilder.where().eq("uuid", UUID).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return alarmModels;
    }


    public void setAlarms(List<AlarmModel> alarmModels){
        for(AlarmModel alarmModel : alarmModels){
            mDatabaseHelper.getAlarmModels().createOrUpdate(alarmModel);
        }
    }

    public void setAlarm(AlarmModel alarmModel){
        mDatabaseHelper.getAlarmModels().create(alarmModel);
    }


}
