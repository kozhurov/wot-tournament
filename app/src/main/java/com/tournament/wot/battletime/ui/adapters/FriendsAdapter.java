package com.tournament.wot.battletime.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.model.players.Friend;

import java.util.List;

public final class FriendsAdapter extends ArrayAdapter<Friend> {

    private final LayoutInflater mLayoutInflater;

    public FriendsAdapter(Context context, List<Friend> pFriendList) {
        super(context, 0, pFriendList);
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Friend friend = getItem(position);
        Holder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.adapter_friends, parent, false);
            holder = new Holder();

            holder.mLastBattleTime = (TextView) convertView.findViewById(R.id.friends_adapter_friend_last_battle);
            holder.mPlayerName = (TextView) convertView.findViewById(R.id.friends_adapter_friend_name);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.mPlayerName.setText(friend.getName());
        holder.mLastBattleTime.setText(friend.getLastBattleDate());

        return convertView;
    }

    private static final class Holder {
        TextView mPlayerName;
        TextView mLastBattleTime;
    }
}
