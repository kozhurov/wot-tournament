package com.tournament.wot.battletime.ui.dialogs;

import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.core.Constants;
import com.tournament.wot.battletime.core.callbacks.DialogsCallbacks;
import com.tournament.wot.battletime.model.players.Friend;
import com.tournament.wot.battletime.model.players.Participant;
import com.tournament.wot.battletime.ui.adapters.FriendsAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class FriendDialog extends GenericDialog {

    private static final String PEOPLE_TYPE = "people_type";
    private static final String PEOPLE_LIMIT_TRANSPORT_KEY = "people_limit";
    private static final String PEOPLE_MAIN_LIMIT_TRANSPORT_KEY = "people_main_limit";

    public static final int MODE_FRIENDS = 0;
    public static final int MODE_CLAN_MEMBER = 1;
    public static final int MODE_COMMANDER = 2;

    private ListView mFriendsListView;
    private DialogsCallbacks mDialogsCallbacks;
    private int mTotalEventCapacity;
    private int mMainEventCapacity;
    private int mPeopleType;

    public static Bundle buildArgs(int pPeopleLimit, int pMainLimit, int pPeopleType) {
        Bundle bundle = new Bundle();
        bundle.putInt(PEOPLE_LIMIT_TRANSPORT_KEY, pPeopleLimit);
        bundle.putInt(PEOPLE_MAIN_LIMIT_TRANSPORT_KEY, pMainLimit);
        bundle.putInt(PEOPLE_TYPE, pPeopleType);
        return bundle;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        mMainEventCapacity = bundle.getInt(PEOPLE_MAIN_LIMIT_TRANSPORT_KEY);
        mTotalEventCapacity = bundle.getInt(PEOPLE_LIMIT_TRANSPORT_KEY);
        mPeopleType = bundle.getInt(PEOPLE_TYPE, Constants.WRONG_ARGS);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_friend, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        mFriendsListView = (ListView) view.findViewById(R.id.friend_dialog_friend_list);

        List<Friend> entities = null;
        switch (mPeopleType) {
            case MODE_COMMANDER:
                getDialog().setTitle(R.string.action_change_commander);
                entities = mActivityBridge.getRestoreManager().loadClanMembers();
                mFriendsListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
                break;

            case MODE_FRIENDS:
                getDialog().setTitle(R.string.create_simple_plan_user_picker);
                entities = mActivityBridge.getRestoreManager().loadFriend();
                break;

            case MODE_CLAN_MEMBER:
                getDialog().setTitle(R.string.create_simple_plan_user_picker);
                entities = mActivityBridge.getRestoreManager().loadClanMembers();
                break;

            default:
                dismiss();
                break;
        }

        mFriendsListView.setAdapter(new FriendsAdapter(getActivity(), entities));
        view.findViewById(R.id.friend_dialog_ok_button).setOnClickListener(new Clicker());

    }

    @Override
    public void setDialogsCallbacks(DialogsCallbacks pDialogsCallbacks) {
        mDialogsCallbacks = pDialogsCallbacks;
    }

    private void finishDialogWithResult() {
        SparseBooleanArray selectedPlayers = mFriendsListView.getCheckedItemPositions();

        List<Integer> realPosition = new ArrayList<Integer>(selectedPlayers.size());
        for (int i = 0; i < selectedPlayers.size(); i++) {
            if (selectedPlayers.valueAt(i)) {
                realPosition.add(selectedPlayers.keyAt(i));
            }
        }

        int selectedSize = realPosition.size();

        if (selectedSize > mTotalEventCapacity) {
            String errorString = getString(R.string.create_simple_plan_to_many_user_selected) + " " + mMainEventCapacity;
            showShortToast(errorString);
            return;
        }


        ArrayAdapter<Friend> adapter = (ArrayAdapter<Friend>) mFriendsListView.getAdapter();
        Collection<Participant> participants = new ArrayList<Participant>(selectedSize);

        if (mPeopleType != MODE_COMMANDER) {
            Friend friend;
            Participant participant;
            for (Integer position : realPosition) {
                friend = adapter.getItem(position);

                participant = new Participant(friend.getId(), friend.getName());

                if (!friend.isRegistered()) {
                    participant.setParticipantAnswer(Participant.OUT_OF_SERVICE);
                }

                participant.setDamageRate(friend.getDamageRate());
                participant.setWinRate(friend.getWinRate());
                participant.setParticipantRole(Constants.WRONG_ARGS);

                participants.add(participant);
            }

            mDialogsCallbacks.friendSelected(participants);

        } else {
            Friend newCommander = null;
            if (realPosition.size() > 0) {
                newCommander = adapter.getItem(realPosition.get(0));
            }
            mDialogsCallbacks.newCommanderSelected(newCommander);
        }

        dismiss();
    }

    private final class Clicker implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.friend_dialog_ok_button:
                    finishDialogWithResult();
                    break;
            }
        }
    }

}
