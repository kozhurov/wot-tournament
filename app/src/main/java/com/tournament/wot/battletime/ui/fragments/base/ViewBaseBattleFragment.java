package com.tournament.wot.battletime.ui.fragments.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.core.Constants;

public final class ViewBaseBattleFragment extends GenericEventFragment {

    public static Bundle buildArgs(long pEventId) {
        Bundle args = new Bundle();
        args.putLong(EVENT_UUID_TRANSPORT_KEY, pEventId);
        return args;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setIsDragEnabled(false);
        setIsRemoveEnabled(false);
        setIsSortEnabled(false);

        long transportKey = getArguments().getLong(EVENT_UUID_TRANSPORT_KEY, Constants.WRONG_ARGS);
        mSimpleBattle = mActivityBridge.getRestoreManager().getBattleModel(transportKey);

        setEventType(mSimpleBattle.getEventType());
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        inflateFooter(R.layout.footer_create_simple_battle, inflater);
        return inflater.inflate(R.layout.fragment_create_simple_battle, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivityBridge.enableBackButton();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.view_simple_plan, menu);
    }
}
