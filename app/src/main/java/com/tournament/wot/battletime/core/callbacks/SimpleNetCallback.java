package com.tournament.wot.battletime.core.callbacks;

import com.tournament.wot.battletime.model.battle.BattleModel;
import com.tournament.wot.battletime.model.players.Friend;
import com.tournament.wot.battletime.model.players.Player;
import com.tournament.wot.battletime.model.tanks.BestTanksModel;

import java.util.ArrayList;
import java.util.List;

public class SimpleNetCallback implements NetCallbacks {

    @Override
    public void taskCompleteWithError(String pErrorCode) {

    }

    @Override
    public void authComplete(String pRedirectUrl) {

    }

    @Override
    public void loadMyInfoCompeted(Player pPlayer) {

    }

    @Override
    public void baseSyncComplete(List<Friend> pFriends, int pTimestamp, Player pPlayer) {

    }

    @Override
    public void battleCreated(BattleModel pBattleModel) {

    }

    @Override
    public void battleUpdated(BattleModel pBattleModel) {

    }

    @Override
    public void incrementSyncComplete(int pTimestamp, List<BattleModel> pBattles, List<Friend> pClanMembers) {

    }

    @Override
    public void loadBestTanks(ArrayList<BestTanksModel> pBestTanksModel) {

    }

    @Override
    public void changeCommanderComplete(long pNewCommanderId, String pEventUUID) {

    }

    @Override
    public void userAnswerAccepted(int pTimestamp, int pAnswerKey) {

    }

    @Override
    public void reSendComplete() {

    }

    @Override
    public void deleteEventComplete() {

    }

}
