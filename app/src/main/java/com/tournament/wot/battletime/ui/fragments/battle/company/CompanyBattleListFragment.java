package com.tournament.wot.battletime.ui.fragments.battle.company;

import android.app.Activity;

import com.tournament.wot.battletime.model.battle.BattleModel;
import com.tournament.wot.battletime.ui.fragments.base.GenericListFragment;

public final class CompanyBattleListFragment extends GenericListFragment {

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setEventType(BattleModel.EventType.COMPANY);
    }
}
