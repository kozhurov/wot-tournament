package com.tournament.wot.battletime.model.alarm;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by bogdan on 27.10.14.
 */
@DatabaseTable
public class AlarmModel {

    @DatabaseField(allowGeneratedIdInsert = true, generatedId =  true)
    private int mId;
    @DatabaseField(columnName = "uuid")
    private String mUUID;
    @DatabaseField(columnName = "day_type",defaultValue = "-1")
    private int mDayType;

    public String getmUUID() {
        return mUUID;
    }

    public void setmUUID(String mUUID) {
        this.mUUID = mUUID;
    }

    public int getmDayType() {
        return mDayType;
    }

    public void setmDayType(int mDayType) {
        this.mDayType = mDayType;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }
}
