package com.tournament.wot.battletime.ui.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.model.battle.BattleModel;
import com.tournament.wot.battletime.model.players.Participant;

import java.util.List;

public final class ClanBattleAdapter extends ArrayAdapter<BattleModel> {

    private static final int CLAN_EVENT_CAPACITY = 20;
    private static final String NULL_STRING = "null";
    private static final String NON_RESULT_STRING = "..-..";
    private final LayoutInflater mLayoutInflater;

    public ClanBattleAdapter(Context context, List<BattleModel> pBattleList) {
        super(context, 0, pBattleList);
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BattleModel battle = getItem(position);
        Holder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.adapter_clan_battle, parent, false);
            holder = new Holder();

            holder.mBattleStartTime = (TextView) convertView.findViewById(R.id.clan_battle_adapter_battle_start_time);
            holder.mBattleName = (TextView) convertView.findViewById(R.id.clan_battle_adapter_battle_name);
            holder.mBattleResult = (TextView) convertView.findViewById(R.id.clan_battle_adapter_battle_result);
            holder.mIsPlayerInvited = (ImageView) convertView.findViewById(R.id.clan_battle_adapter_player_is_invite);
            holder.mPlayerAnswer = (ImageView) convertView.findViewById(R.id.clan_battle_adapter_player_answer_status);
            holder.mEventFilling = (TextView) convertView.findViewById(R.id.clan_battle_adapter_battle_filling);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.mBattleName.setText(battle.getName());
        holder.mBattleStartTime.setText(battle.getStartBattleDateString());

        String result = battle.getBattleResult();
        if (TextUtils.isEmpty(result) || result.equals(NULL_STRING)) {
            holder.mBattleResult.setText(NON_RESULT_STRING);
        } else {
            holder.mBattleResult.setText(result);
        }

        if (battle.isInvited()) {
            holder.mIsPlayerInvited.setImageResource(R.drawable.ic_invited);

            int response;
            Log.e("CBA", "response = "+battle.getUserAnswer());
            if(battle.getUserAnswer() == Participant.NON_ANSWER) {
              response = R.drawable.ic_your_non_answer;
            } else {
                response = R.drawable.ic_you_answer;
            }

            holder.mPlayerAnswer.setImageResource(response);
        } else {
            holder.mIsPlayerInvited.setImageResource(R.drawable.ic_non_invited);
        }

        String filling = (battle.getParticipantsFromBase().size()) + " / " + CLAN_EVENT_CAPACITY;
        holder.mEventFilling.setText(filling);

        return convertView;
    }

    private static final class Holder {
        TextView mBattleName;
        TextView mBattleStartTime;
        ImageView mIsPlayerInvited;
        ImageView mPlayerAnswer;
        TextView mBattleResult;
        TextView mEventFilling;
    }
}
