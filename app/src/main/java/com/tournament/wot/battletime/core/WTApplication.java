package com.tournament.wot.battletime.core;

import android.app.Application;
import android.content.ComponentCallbacks2;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.tournament.wot.battletime.net.NetManager;
import com.tournament.wot.battletime.restore.RestoreManager;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public final class WTApplication extends Application {

    private Executor mExecutor;
    private NetManager mNetManager;
    private SharedPref mSharedPref;
    private RestoreManager mRestoreManager;
    private SupportTaskManager mSupportTaskManager;
    private boolean mIsActivityStarted;

    @Override
    public void onCreate() {
        super.onCreate();
        mExecutor = Executors.newSingleThreadExecutor();
        mNetManager = new NetManager(mExecutor);
        mSharedPref = new SharedPref(getApplicationContext());
        mRestoreManager = new RestoreManager(this);
        mSupportTaskManager = new SupportTaskManager(mExecutor);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).threadPoolSize(3).build();
        ImageLoader.getInstance().init(config);
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        if(level  == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN){
            mIsActivityStarted = false;
        }
    }

    public SharedPref getSharedPref() {
        return mSharedPref;
    }

    public boolean isActivityStarted() {
        return mIsActivityStarted;
    }

    public void setIsActivityStarted(boolean mIsActivityStarted) {
        this.mIsActivityStarted = mIsActivityStarted;
    }

    public NetManager getNetManager() {
        return mNetManager;
    }

    public RestoreManager getRestoreManager() {
        return mRestoreManager;
    }

    public SupportTaskManager getSupportTaskManager() {
        return mSupportTaskManager;
    }
}
