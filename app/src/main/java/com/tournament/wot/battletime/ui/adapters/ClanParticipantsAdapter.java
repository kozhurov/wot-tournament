package com.tournament.wot.battletime.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.model.players.Friend;

import java.util.List;

public class ClanParticipantsAdapter extends ArrayAdapter<Friend> {

    private final LayoutInflater mLayoutInflater;

    public ClanParticipantsAdapter(Context context, List<Friend> pParticipants) {
        super(context, R.layout.adapter_clan_owner_spinner, pParticipants);
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return iniView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return iniView(position, convertView, parent);
    }

    private View iniView(int position, View convertView, ViewGroup parent) {
        Friend participant;
        Holder holder;

        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.adapter_clan_owner_spinner, parent, false);
            holder = new Holder();

            holder.mName = (TextView) convertView.findViewById(R.id.clan_spinner_participant_name);
            holder.mWinRate = (TextView) convertView.findViewById(R.id.clan_spinner_participant_win_rate);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        participant = getItem(position);

        holder.mName.setText(participant.getName());
        holder.mWinRate.setText(String.valueOf(participant.getWinRate()));

        return convertView;
    }

    private static final class Holder {
        TextView mName;
        TextView mWinRate;
    }
}
