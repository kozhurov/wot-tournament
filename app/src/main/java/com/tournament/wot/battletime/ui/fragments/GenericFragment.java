package com.tournament.wot.battletime.ui.fragments;


import android.app.Activity;
import android.app.Fragment;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.core.bridges.ActivityBridge;

public abstract class GenericFragment extends Fragment {

    protected ActivityBridge mActivityBridge;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivityBridge = (ActivityBridge) activity;
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(getActivity()).set(Fields.SCREEN_NAME, getClass().getSimpleName());
        EasyTracker.getInstance(getActivity()).send(MapBuilder.createAppView().build());
    }

    public void showShortToast(int pResId) {
        showShortToast(getString(pResId));
    }

    public void showShortToast(String pStringBody) {
        Toast.makeText(getActivity(), pStringBody, Toast.LENGTH_SHORT).show();
    }
    public void showLongToast(int pResId) {
        showLongToast(getString(pResId));
    }

    public void showLongToast(String pStringBody) {
        Toast.makeText(getActivity(), pStringBody, Toast.LENGTH_LONG).show();
    }

    public void showNoInternetToast() {
        Toast.makeText(getActivity(), R.string.no_internet_error, Toast.LENGTH_SHORT).show();
    }

    public void showNoInfoToast() {
        Toast.makeText(getActivity(), R.string.no_data_error, Toast.LENGTH_SHORT).show();
    }

    public void showShortToastFromBackground(final String pStringBody) {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Toast.makeText(getActivity(), pStringBody, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showShortToastFromBackground(final int pResId) {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Toast.makeText(getActivity(), pResId, Toast.LENGTH_SHORT).show();
            }
        });
    }

}
