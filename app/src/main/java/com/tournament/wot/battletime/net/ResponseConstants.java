package com.tournament.wot.battletime.net;

final class ResponseConstants {

    static final String WG_DATA_OBJECT_KEY = "data";

    static final String BT_RESULT_BASE_SYNC_DATA_OBJECT_KEY = "SyncUserDataResult";
    static final String BT_RESULT_INCREMENT_SYNC_DATA_OBJECT_KEY = "IncrementSyncResult";
    static final String BT_RESULT_SIMPLE_BATTLE_CREATE_KEY = "CreateEventResult";
    static final String BT_RESULT_BEST_TANKS_KEY = "GetBestTanksResult";
    static final String BT_USER_ANSWER_RESULT = "SetUserAnswerResult";

    static final String BT_DATA_CLAN_MEMBERS = "ClanMembersData";
    static final String BT_DATA_CLAN_EVENTS = "EventClanWarsData";
    static final String BT_DATA_COMPANY_EVENTS = "EventCompanysData";
    static final String BT_DATA_PLATOONS_EVENTS = "EventPlatoonsData";
    static final String BT_DATA_TRAINING_EVENTS = "TrainingsData";
    static final String BT_DATA_FRIENDS = "FriendsData";

    static final String BT_DATA_BEST = "BestTanks";

    static final String LOCATION_KEY = "location";
    static final String BASE_SYNC_FRIENDS_ARRAY = "FriendsInfo";
    static final String BASE_SYNC_USER_OBJECT = "UserInfo";
    static final String UNIVERSAL_TIMESTAMP = "TimeStamp";

}