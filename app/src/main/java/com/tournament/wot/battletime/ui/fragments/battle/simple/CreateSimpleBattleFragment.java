package com.tournament.wot.battletime.ui.fragments.battle.simple;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;

import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.model.battle.BattleModel;
import com.tournament.wot.battletime.ui.fragments.base.GenericEventFragment;

public final class CreateSimpleBattleFragment extends GenericEventFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
        setEventType(BattleModel.EventType.PLATOON);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.ceate_simple_battle, menu);
    }
}