package com.tournament.wot.battletime.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.core.Constants;
import com.tournament.wot.battletime.model.tanks.BestTanksModel;

import java.util.List;

public final class BestTankAdapter extends ArrayAdapter<BestTanksModel> {

    private final LayoutInflater mLayoutInflater;
    private final DisplayImageOptions mDisplayImageOptions;

    public BestTankAdapter(Context context, List<BestTanksModel> pTanksModels) {
        super(context, 0, pTanksModels);
        mLayoutInflater = LayoutInflater.from(context);
        mDisplayImageOptions = new DisplayImageOptions.Builder().cacheOnDisk(true).resetViewBeforeLoading(true).build();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BestTanksModel bestTanksModel = getItem(position);
        ImageLoader imageLoader = ImageLoader.getInstance();
        Holder holder;

        if (convertView == null) {
            holder = new Holder();
            convertView = mLayoutInflater.inflate(R.layout.adapter_best_tank, parent, false);

            holder.tankImage = (ImageView) convertView.findViewById(R.id.last_day_fragment_tank_image);
            holder.tankName = (TextView) convertView.findViewById(R.id.last_day_fragment_tank_name);
            holder.winRate = (TextView) convertView.findViewById(R.id.last_day_fragment_rate_win);
            holder.damageRate = (TextView) convertView.findViewById(R.id.last_day_fragment_rate_damage);
            holder.expRate = (TextView) convertView.findViewById(R.id.last_day_fragment_rate_exp);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        imageLoader.displayImage(bestTanksModel.getImageUrl(), holder.tankImage, mDisplayImageOptions);
        holder.tankName.setText(bestTanksModel.getTitle());

        String damage = getContext().getString(R.string.damage_rate) + Constants.MIDDLE_LITERAL + bestTanksModel.getDamageRate();
        holder.damageRate.setText(damage);

        String win = getContext().getString(R.string.win_rate) + Constants.MIDDLE_LITERAL + bestTanksModel.getWinRate();
        holder.winRate.setText(win);

        String exp = getContext().getString(R.string.exp_rate) + Constants.MIDDLE_LITERAL + bestTanksModel.getAmountExp();
        holder.expRate.setText(exp);

        return convertView;
    }

    private static final class Holder {
        ImageView tankImage;
        TextView tankName;
        TextView winRate;
        TextView damageRate;
        TextView expRate;
    }
}
