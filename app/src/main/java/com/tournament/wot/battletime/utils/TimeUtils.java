package com.tournament.wot.battletime.utils;

import java.util.Date;

public final class TimeUtils {

    private static final long ONE_MIN = 60000;
    private static final long FIVE_MIN = 300000;
    private static final long TWENTY_MIN = 1200000;

    public static boolean isTokenDie(int pDieTime) {
        long currentTime = System.currentTimeMillis() / 1000;

        if (pDieTime > currentTime) {
            return false;
        }
        return true;
    }

    public static boolean isAcceptResend(long pLatReSend) {
        return (System.currentTimeMillis() - pLatReSend) > ONE_MIN;
    }

    public static boolean isEventInFuture(Date pDate) {
        if (pDate != null) {
            return System.currentTimeMillis() < pDate.getTime();
        } else {
            return true;
        }
    }

    public static Date setDefaultStartDate() {
        long newTime = new Date().getTime() + FIVE_MIN;
        return new Date(newTime);
    }

    public static Date setDefaultEndDate() {
        long newTime = new Date().getTime() + TWENTY_MIN;
        return new Date(newTime);
    }
}
