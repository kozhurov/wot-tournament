package com.tournament.wot.battletime.ui.dialogs;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.model.battle.BattleModel;

public final class AlarmDialog extends GenericDialog {

    private static final String TRANSIT_EVENT_ID = "transit_event_id";

    public static final Bundle buildArgs(long pId){
        Bundle bundle = new Bundle();
        bundle.putLong(TRANSIT_EVENT_ID, pId);
        return bundle;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_alarm, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        getDialog().setTitle(R.string.alarm_dialog_title);
        long id = getArguments().getLong(TRANSIT_EVENT_ID);
        BattleModel battleModel = mActivityBridge.getRestoreManager().getBattleModel(id);

        if(battleModel == null){
            return;
        }

        TextView mainTextView = (TextView) view.findViewById(R.id.dialog_alarm_main_text);
        String message = battleModel.getName() + Html.fromHtml(getResources().getString(R.string.alarm_current_event));
        mainTextView.setText(message);

        view.findViewById(R.id.dialog_alarm_button_ok).setOnClickListener(new Clicker());
    }


    private final class Clicker implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            dismiss();
        }
    }
}
