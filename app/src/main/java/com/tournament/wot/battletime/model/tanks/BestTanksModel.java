package com.tournament.wot.battletime.model.tanks;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.json.JSONException;
import org.json.JSONObject;

@DatabaseTable(tableName = "best_tank")
public class BestTanksModel {

    static final String TAG_DAMAGE_RATE = "DamadgeRate";
    static final String TAG_EXP_RATE = "ExpRate";
    static final String TAG_WIN_RATE = "WinRate";
    static final String TAG_IMAGE_URL = "ImgUrl";
    static final String TAG_NAME = "Name";
    static final String TAG_OWNER_ID = "Owner";
    static final String TAG_TANK_TYPE = "TankType";
    static final String TAG_TANK_UIID = "TankUuid";

    @DatabaseField(id = true)
    private int mType;

    @DatabaseField
    private String mTitle;

    @DatabaseField
    private String mInfo;

    @DatabaseField
    private int mWinRate;

    @DatabaseField
    private int mDamageRate;

    @DatabaseField
    private int mAmountSilver;

    @DatabaseField
    private int mAmountExp;

    @DatabaseField
    private String mImageUrl;

    public BestTanksModel() {

    }

    public BestTanksModel(JSONObject jsonObject) {
        try {
            mType = jsonObject.getInt(TAG_TANK_TYPE);
            mTitle = jsonObject.getString(TAG_NAME);

            mDamageRate = jsonObject.getInt(TAG_DAMAGE_RATE);
            mWinRate = jsonObject.getInt(TAG_WIN_RATE);
            mAmountExp = jsonObject.getInt(TAG_EXP_RATE);

            mImageUrl = jsonObject.getString(TAG_IMAGE_URL);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getType() {
        return mType;
    }

    public void setType(int mId) {
        this.mType = mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getInfo() {
        return mInfo;
    }

    public void setInfo(String mInfo) {
        this.mInfo = mInfo;
    }

    public int getWinRate() {
        return mWinRate;
    }

    public void setWinRate(int mWinRate) {
        this.mWinRate = mWinRate;
    }

    public int getDamageRate() {
        return mDamageRate;
    }

    public void setDamageRate(int mDamageRate) {
        this.mDamageRate = mDamageRate;
    }

    public int getAmountSilver() {
        return mAmountSilver;
    }

    public void setAmountSilver(int mAmountSilver) {
        this.mAmountSilver = mAmountSilver;
    }

    public int getAmountExp() {
        return mAmountExp;
    }

    public void setAmountExp(int mAmountExp) {
        this.mAmountExp = mAmountExp;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String pImageUrl) {
        mImageUrl = pImageUrl;
    }
}
