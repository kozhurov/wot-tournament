package com.tournament.wot.battletime.ui.fragments;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.ui.Launcher;
import com.tournament.wot.battletime.ui.adapters.DrawerAdapter;

import java.util.ArrayList;
import java.util.List;

public final class DrawerFragment extends GenericFragment {

    private ListView mCategoriesListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_drawer, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mCategoriesListView = (ListView) view.findViewById(R.id.drawer_fragment_category_list);
        mCategoriesListView.setAdapter(new DrawerAdapter(getActivity(), getDrawerList()));
        mCategoriesListView.setOnItemClickListener(new ListClicker());
        mCategoriesListView.setItemChecked(0,true);
    }

    private List<String> getDrawerList() {
        Resources resources = getResources();
        List<String> categories = new ArrayList<String>();
        categories.add(resources.getString(R.string.categories_simple_battle));
        categories.add(resources.getString(R.string.categories_company_battle));
        categories.add(resources.getString(R.string.categories_clan_battle));
        categories.add(resources.getString(R.string.categories_training_battle));
        categories.add(resources.getString(R.string.categories_last_day));

        return categories;
    }

    private final class ListClicker implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Launcher launcher = mActivityBridge.getMainLauncher();
            switch (position) {
                case 0:
                    launcher.launchSimpleBattleListFragment();
                    break;

                case 1:
                    launcher.launchCompanyBattleListFragment();
                    break;

                case 2:
                    launcher.launchClanBattleListFragment();
                    break;

                case 3:
                    launcher.launchTrainingBattleFragment();
                    break;

                case 4:
                    launcher.launchBestTankFragment();
                    break;
            }

            mActivityBridge.closeDrawer();
        }
    }
}
