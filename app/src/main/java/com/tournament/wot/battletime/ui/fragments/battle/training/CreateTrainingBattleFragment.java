package com.tournament.wot.battletime.ui.fragments.battle.training;

import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.core.Constants;
import com.tournament.wot.battletime.model.battle.BattleModel.EventType;
import com.tournament.wot.battletime.model.players.Participant;
import com.tournament.wot.battletime.model.players.Player;
import com.tournament.wot.battletime.ui.adapters.comparator.ParticipantComparator;
import com.tournament.wot.battletime.ui.fragments.base.GenericEventFragment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class CreateTrainingBattleFragment extends GenericEventFragment {

    private ActionMode actionMode;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
        setEventType(EventType.TRAINING);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        inflateFooter(R.layout.footer_create_simple_battle, inflater);
        return inflater.inflate(R.layout.fragment_create_simple_battle, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDragSortListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        mDragSortListView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                actionMode = mode;
                Participant participant = participantsAdapter.getItem(position);
                if (checked == true) {
                    participant.setPrevioseRole(participant.getParticipantRole());
                    participant.setParticipantRole(4);
                } else {
                    participant.setParticipantRole(participant.getPrevioseRole());
                }
                participantsAdapter.notifyDataSetChanged();
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.training_action_mode, menu);
                actionMode = mode;
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.training_action_one_main:
                        changeSelectedItems(Participant.MAIN_PARTICIPANT);
                        break;
                    case R.id.training_action_one_slave:
                        changeSelectedItems(Participant.RESERVE_PARTICIPANT);
                        break;

                    case R.id.training_action_two_main:
                        changeSelectedItems(Participant.MAIN_SECONDARY_TEAM_PARTICIPANT);
                        break;

                    case R.id.training_action_two_slave:
                        changeSelectedItems(Participant.RESERVE_SECONDARY_TEAM_PARTICIPANT);
                        break;

                    default:
                        Log.e("onActionItemClicked", "default");
                        break;
                }

                mode.finish();
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                actionMode = null;
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.create_training_plan, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_save_simple_plan:
                clearUnActiveParticipant();
                super.onOptionsItemSelected(item);
                break;

            case R.id.training_action_sort:
                Player player = mActivityBridge.getRestoreManager().loadMe();
                participantsAdapter.sort(new ParticipantComparator(player.getId()));
                break;

        }

        return false;
    }

    private void changeSelectedItems(int type) {
        int size = participantsAdapter.getCount();
        for (int i = 0; i < size; i++) {
            Participant participant = participantsAdapter.getItem(i);
            if (participant.getParticipantRole() == 4) {
                participant.setParticipantRole(type);
            }
        }
        participantsAdapter.notifyDataSetChanged();
    }

    private void clearUnActiveParticipant() {
        List<Participant> participantsToRemoveList = new ArrayList<Participant>();
        Collection<Participant> participants = mSimpleBattle.getParticipants();
        for (Participant participant : participants) {
            Log.e("CT", "id = " + participant.getParticipantId() + " role = " + participant.getParticipantRole());
            if (participant.getParticipantRole() == Constants.WRONG_ARGS) {
                participantsToRemoveList.add(participant);
            }
        }

        participants.removeAll(participantsToRemoveList);

    }
}
