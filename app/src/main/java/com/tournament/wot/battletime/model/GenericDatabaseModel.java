package com.tournament.wot.battletime.model;

import com.j256.ormlite.field.DatabaseField;

public class GenericDatabaseModel {

    @DatabaseField(id = true)
    private long mId;

    public long getId() {
        return mId;
    }

    public void setId(long pId) {
        mId = pId;
    }

}
