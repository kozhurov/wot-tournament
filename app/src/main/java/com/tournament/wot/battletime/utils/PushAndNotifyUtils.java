package com.tournament.wot.battletime.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.TextUtils;
import android.util.Log;

import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.core.WTApplication;
import com.tournament.wot.battletime.model.battle.BattleModel;
import com.tournament.wot.battletime.ui.activitys.MainActivity;

public final class PushAndNotifyUtils {

    public static final String MESSAGE_KEY = "Message";
    public static final String EVENT_ID_KEY = "EventId";

    private static final String INVITE_TEXT = "You invited to take part in";
    private static final String DELETE_TEXT = "is no longer available";
    public static final String ALARM_TEXT = "must go to event";

    private static final int NOTIFY_NUMBER = 11;

    private static boolean isNewEventInvite(String pIncomeString) {
        return (!TextUtils.isEmpty(pIncomeString) && pIncomeString.startsWith(INVITE_TEXT));
    }

    public static boolean isEventDelete(String pIncomeString) {
        return (!TextUtils.isEmpty(pIncomeString) && pIncomeString.endsWith(DELETE_TEXT));
    }

    public static boolean isAlarm(String pIncomeString) {
        return (!TextUtils.isEmpty(pIncomeString) && pIncomeString.startsWith(ALARM_TEXT));
    }

    public static void showNotification(Context pContext, Bundle pBundle) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(pContext);
        builder.setSmallIcon(R.drawable.logotype);

        String content = pBundle.getString(PushAndNotifyUtils.MESSAGE_KEY);
        String contentTitle = buildContentString(pContext, content, pBundle);

        if (!TextUtils.isEmpty(contentTitle)) {
            builder.setContentTitle(contentTitle);
        }

        if (isAlarm(content)) {
            long id = pBundle.getLong(EVENT_ID_KEY);
            BattleModel model = ((WTApplication) pContext.getApplicationContext()).getRestoreManager().getBattleModel(id);

            if (model != null) {
                String subtitle = pContext.getResources().getString(R.string.alarm_dialog_battle_name);
                builder.setContentText(subtitle + " " + model.getName());
            }
        }

        Intent resultIntent = new Intent(pContext, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(pContext);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);

        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) pContext.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(NOTIFY_NUMBER, builder.build());

    }

    private static String buildContentString(Context pContext, String pContent, Bundle pBundle) {
        Resources resources = pContext.getResources();

        if (isNewEventInvite(pContent)) {
            return resources.getString(R.string.push_invite_to_event);
        }

        if (isEventDelete(pContent)) {
            String eventToDeleteUUID = pBundle.getString(PushAndNotifyUtils.EVENT_ID_KEY);
            ((WTApplication) pContext.getApplicationContext()).getRestoreManager().deleteBattleModel(eventToDeleteUUID);
            return resources.getString(R.string.push_delete_event);
        }

        if (isAlarm(pContent)) {
            return resources.getString(R.string.alarm_point_event);
        }

        return resources.getString(R.string.push_to_event);
    }
}
