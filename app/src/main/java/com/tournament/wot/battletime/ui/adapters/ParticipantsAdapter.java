package com.tournament.wot.battletime.ui.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.model.players.Participant;
import com.tournament.wot.battletime.utils.TextViewUtils;

import java.util.Collection;
import java.util.List;

public class ParticipantsAdapter extends ArrayAdapter<Participant> {

    public static final int TRAINING_MODE = 0;
    public static final int OTHERS_MODE = 1;
    private final LayoutInflater mLayoutInflater;
    private final int mMode;
    private final int mMainParticipantsCapacity;

    public ParticipantsAdapter(Context pContext, Collection<Participant> pParticipants, int pMode, int pMainParticipantsCapacity) {
        super(pContext, 0, (List<Participant>) pParticipants);
        mMode = pMode;
        mMainParticipantsCapacity = pMainParticipantsCapacity;
        mLayoutInflater = LayoutInflater.from(pContext);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        Participant participant = getItem(position);

        if (convertView == null) {
            holder = new Holder();
            convertView = mLayoutInflater.inflate(R.layout.adapter_participants, parent, false);

            holder.mName = (TextView) convertView.findViewById(R.id.participants_adapter_participant_name);
            holder.mCount = (TextView) convertView.findViewById(R.id.participants_count);
            holder.mStatus = (ImageView) convertView.findViewById(R.id.participants_adapter_participant_status);
            holder.mWinRate = (TextView) convertView.findViewById(R.id.participants_adapter_participant_win_rate);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        Resources resources = getContext().getResources();

        TextViewUtils.CheckView(participant.getParticipantName(), holder.mName);
        TextViewUtils.CheckView(String.valueOf(position + 1), holder.mCount);
        TextViewUtils.CheckView(resources.getString(R.string.win_rate) + " " + participant.getWinRate() + " %", holder.mWinRate);

        int localWinRate = participant.getWinRate();

        if (localWinRate < 40) {
            holder.mWinRate.setTextColor(resources.getColor(R.color.permanent_player_color));
        } else if (localWinRate > 40 && localWinRate < 51) {
            holder.mWinRate.setTextColor(resources.getColor(R.color.middle_player_color));
        } else if (localWinRate > 50 && localWinRate < 61) {
            holder.mWinRate.setTextColor(resources.getColor(R.color.ac_player_color));
        } else if (localWinRate > 60 && localWinRate < 71) {
            holder.mWinRate.setTextColor(resources.getColor(R.color.sky_hight_player_color));
        } else if (localWinRate > 70) {
            holder.mWinRate.setTextColor(resources.getColor(R.color.legendary_player_color));
        }

        switch (mMode) {
            case TRAINING_MODE:
                makeColorByRole(convertView, participant);
                break;

            case OTHERS_MODE:
                makeColorByCapacity(convertView, position);
                break;
        }


        switch (participant.getParticipantAnswer()) {

            case Participant.MAY_BE_ANSWER:
                holder.mStatus.setImageResource(R.drawable.ic_may_be);
                break;

            case Participant.POSITIVE_ANSWER:
                holder.mStatus.setImageResource(R.drawable.ic_check);
                break;

            case Participant.NEGATIVE_ANSWER:
                holder.mStatus.setImageResource(R.drawable.ic_reject);
                break;

            case Participant.NON_ANSWER:
                holder.mStatus.setImageResource(R.drawable.ic_non_answer);
                break;

            case Participant.OUT_OF_SERVICE:
                holder.mStatus.setImageResource(R.drawable.ic_unregister_user);
                break;
        }

        return convertView;
    }

    private void makeColorByRole(View convertView, Participant pParticipant) {
        switch (pParticipant.getParticipantRole()) {
            case 0:
                convertView.setBackgroundResource(R.color.master_one_participant_color);
                break;
            case 1:
                convertView.setBackgroundResource(R.color.slave_one_participant_color);
                break;

            case 2:
                convertView.setBackgroundResource(R.color.master_two_participant_color);
                break;

            case 3:
                convertView.setBackgroundResource(R.color.slave_two_participant_color);
                break;

            case 4:
                convertView.setBackgroundResource(R.color.training_temprary);
                break;

            default:
                convertView.setBackgroundResource(R.color.secondary_participant_color);
                break;
        }
    }

    private void makeColorByCapacity(View convertView, int pPosition) {
        if (pPosition + 1 > mMainParticipantsCapacity) {
            convertView.setBackgroundResource(R.color.secondary_participant_color);
            getItem(pPosition).setParticipantRole(Participant.RESERVE_PARTICIPANT);
        } else {
            convertView.setBackgroundResource(R.color.master_one_participant_color);
            getItem(pPosition).setParticipantRole(Participant.MAIN_PARTICIPANT);
        }
    }

    private static final class Holder {
        TextView mName;
        TextView mCount;
        TextView mWinRate;
        ImageView mStatus;
    }
}
