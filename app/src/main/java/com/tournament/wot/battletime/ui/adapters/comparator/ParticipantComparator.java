package com.tournament.wot.battletime.ui.adapters.comparator;

import com.tournament.wot.battletime.model.players.Participant;

import java.util.Comparator;

public final class ParticipantComparator implements Comparator<Participant> {

    private final long mPlayerId;

    public ParticipantComparator(long pAccountId) {
        //mPlayerId = pAccountId;
        mPlayerId = 0;
    }

    @Override
    public int compare(Participant lhs, Participant rhs) {

        if (lhs.getParticipantId() == mPlayerId && lhs.getParticipantRole() == Participant.MAIN_PARTICIPANT) {
            return -1;
        }

        if (rhs.getParticipantId() == mPlayerId && lhs.getParticipantRole() == Participant.MAIN_PARTICIPANT) {
            return 1;
        }

        return lhs.getParticipantRole() - rhs.getParticipantRole();
    }
}
