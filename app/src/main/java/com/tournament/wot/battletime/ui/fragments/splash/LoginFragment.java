package com.tournament.wot.battletime.ui.fragments.splash;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.core.SharedPref;
import com.tournament.wot.battletime.core.callbacks.SimpleNetCallback;
import com.tournament.wot.battletime.model.players.Friend;
import com.tournament.wot.battletime.model.players.Player;
import com.tournament.wot.battletime.restore.RestoreManager;
import com.tournament.wot.battletime.ui.activitys.AuthConfirmActivity;
import com.tournament.wot.battletime.ui.fragments.GenericFragment;
import com.tournament.wot.battletime.utils.SystemUtils;
import com.tournament.wot.battletime.utils.TimeUtils;

import java.util.List;

public final class LoginFragment extends GenericFragment {

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activity.getActionBar().hide();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        View.OnClickListener onClickListener = new Clicker();
        view.findViewById(R.id.drawer_adapter_category_ico).setOnClickListener(onClickListener);
        mActivityBridge.disableDrawer();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        SharedPref sharedPref = mActivityBridge.getSharedPref();
        String token = sharedPref.getAccessToken();
        int tokenDieTime = sharedPref.getTokenDieTime();

        if (TextUtils.isEmpty(token) || TimeUtils.isTokenDie(tokenDieTime)) {
            return;
        }

        Player me = mActivityBridge.getRestoreManager().loadMe();
        boolean isInternetConnected = SystemUtils.isNetworkConnected(getActivity());

        if (me == null && !isInternetConnected) {
            showNoInternetToast();
            return;

        }

        if (me != null && !isInternetConnected) {
            mActivityBridge.getMainLauncher().launchSimpleBattleListFragment();
            showShortToast(R.string.not_full_functionality_error);
            return;
        }

        if (me != null) {
            mActivityBridge.showProgressBar();
            long accountId = me.getId();
            mActivityBridge.getNetManager().loadMyInfo(new InternetCallback(), accountId, token);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK) {
            mActivityBridge.hideProgressBar();
            return;
        }

        switch (requestCode) {
            case AuthConfirmActivity.THIS_ACTIVITY_START_KEY:
                SharedPref sharedPref = mActivityBridge.getSharedPref();

                long accountId = AuthConfirmActivity.parseAndSaveResult(data, sharedPref);
                String authToken = sharedPref.getAccessToken();
                mActivityBridge.getNetManager().loadMyInfo(new InternetCallback(), accountId, authToken);
                break;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getActivity().getActionBar().show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getResources().getBoolean(R.bool.is_tab_orientation)) {
            mActivityBridge.switchToMainTabMode();
        }
    }

    private final class Clicker implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.drawer_adapter_category_ico:

                    if (!SystemUtils.isNetworkConnected(getActivity())) {
                        showNoInternetToast();
                        return;
                    }

                    mActivityBridge.getNetManager().makeAuthorisation(new InternetCallback());
                    mActivityBridge.showProgressBar();
                    break;
            }
        }
    }

    private final class InternetCallback extends SimpleNetCallback {

        @Override
        public void authComplete(final String pRedirectUrl) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mActivityBridge.getMainLauncher().launchAutConfirmActivity(LoginFragment.this, pRedirectUrl);
                }
            });
        }

        @Override
        public void loadMyInfoCompeted(Player pPlayer) {
            Player player = mActivityBridge.getRestoreManager().loadMe();
            if (player == null || pPlayer.getUpdateAt() != player.getUpdateAt()) {

                mActivityBridge.getRestoreManager().saveMe(pPlayer);

                SharedPref sharedPref = mActivityBridge.getSharedPref();
                int tempTimestamp = sharedPref.getBaseSyncTimestamp();
                String deviceId = sharedPref.getDeviceId();
                mActivityBridge.getNetManager().baseSyncProcedure(this, pPlayer, tempTimestamp, deviceId);
            } else {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        mActivityBridge.hideProgressBar();
                        mActivityBridge.getMainLauncher().launchSimpleBattleListFragment();
                    }
                });
            }
        }

        @Override
        public void baseSyncComplete(List<Friend> pFriends, int pTimestamp, Player pPlayer) {

            RestoreManager restoreManager = mActivityBridge.getRestoreManager();
            restoreManager.saveMe(pPlayer);
            restoreManager.saveFriends(pFriends);

            mActivityBridge.getSharedPref().setBaseSyncTimestamp(pTimestamp);

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mActivityBridge.hideProgressBar();
                    mActivityBridge.getMainLauncher().launchSimpleBattleListFragment();
                }
            });
        }

        @Override
        public void taskCompleteWithError(final String pErrorCode) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mActivityBridge.hideProgressBar();
                    showShortToast(pErrorCode);
                }
            });
        }
    }
}
