package com.tournament.wot.battletime.core.bridges;


import com.tournament.wot.battletime.core.SharedPref;
import com.tournament.wot.battletime.core.SupportTaskManager;
import com.tournament.wot.battletime.net.NetManager;
import com.tournament.wot.battletime.restore.RestoreManager;
import com.tournament.wot.battletime.ui.Launcher;

public interface ActivityBridge {

    public NetManager getNetManager();

    public Launcher getMainLauncher();

    public SharedPref getSharedPref();

    public RestoreManager getRestoreManager();

    public SupportTaskManager getSupportTaskManager();

    public void disableDrawer();

    public void enableDrawer();

    public void closeDrawer();

    public void showProgressBar();

    public void hideProgressBar();

    public void disableBackButton();

    public void enableBackButton();

    public void switchToMainTabMode();
}
