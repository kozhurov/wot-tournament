package com.tournament.wot.battletime.net;

final class ApiConstants {

    static final String CONTENT_TYPE_KEY = "content-type";
    static final String CONTENT_TYPE_VALUE = "application/json";
    static final String APP_ID = "797d4b83604af2d446e9ad44783d10b5";

    static final String LANGUAGE_TYPE_RU = "ru";
    static final String DISPLAY_TYPE_DIALOG = "popup";

    static final String IN_GARAGE = "1";
    static final String TANK_FIELD = "tank_id";
    static final String MAP_ID_KEY = "MapId";
    static final String MAP_ID_VALUE = "globalmap";
    static final String PLAYER_FIELDS_TANKS_STRING = "tank_id";
    static final String ACCOUNT_ID = "account_id";
    static final String ACCESSE_TOKEN = "accesse_token";
    private static final String PLAYER_FRIENDS_IDS = "private.friends";
    private static final String PLAYER_NAME = "nickname";
    private static final String PLAYER_LAST_BATTLE_TIME = "last_battle_time";
    private static final String PLAYER_UPDATED_AT = "updated_at";
    private static final String PLAYER_CLAN_ID = "clan_id";
    static final String PLAYER_FIELDS_STRING =
            PLAYER_FRIENDS_IDS + "," + PLAYER_NAME + "," + PLAYER_LAST_BATTLE_TIME + ","
                    + PLAYER_UPDATED_AT + "," + PLAYER_CLAN_ID;
}
