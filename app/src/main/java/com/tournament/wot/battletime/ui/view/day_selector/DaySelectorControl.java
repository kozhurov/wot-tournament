package com.tournament.wot.battletime.ui.view.day_selector;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.tournament.wot.battletime.R;

import java.util.ArrayList;
import java.util.Collection;

public final class DaySelectorControl extends LinearLayout {

    private static final int DAY_IN_WEEK = 7;
    private CheckBox[] mDaySelector;

    public DaySelectorControl(Context context) {
        super(context);
        initComponent();
    }

    public DaySelectorControl(Context context, AttributeSet attrs) {
        super(context, attrs);
        initComponent();
    }

    private void initComponent() {
        View view = inflate(getContext(), R.layout.view_day_selector, this);

        mDaySelector = new CheckBox[DAY_IN_WEEK];
        mDaySelector[0] = (CheckBox) view.findViewById(R.id.view_day_selector_su);
        mDaySelector[1] = (CheckBox) view.findViewById(R.id.view_day_selector_mo);
        mDaySelector[2] = (CheckBox) view.findViewById(R.id.view_day_selector_tu);
        mDaySelector[3] = (CheckBox) view.findViewById(R.id.view_day_selector_wed);
        mDaySelector[4] = (CheckBox) view.findViewById(R.id.view_day_selector_thu);
        mDaySelector[5] = (CheckBox) view.findViewById(R.id.view_day_selector_fri);
        mDaySelector[6] = (CheckBox) view.findViewById(R.id.view_day_selector_sa);
    }

    public Collection<Integer> getSelectedDay() {
        Collection<Integer> repeatDayCode = new ArrayList<Integer>();

        int syncLength = mDaySelector.length;

        for (int i = 0; i < syncLength; i++) {
            if (mDaySelector[i].isChecked()) {
                repeatDayCode.add(i);
            }
        }

        return repeatDayCode;
    }

    public void setSelectedDay(Collection<Integer> pSelectedDay) {

        for (Integer integer : pSelectedDay) {
            mDaySelector[integer].setChecked(true);
        }
    }

    public void disableDaySelector(){
       changeDaySelectorState(false);
    }

    public void enableDaySelector(){
        changeDaySelectorState(true);
    }

    private void changeDaySelectorState(boolean b){
        for(CheckBox checkBox : mDaySelector){
            checkBox.setEnabled(b);
        }
    }
}
