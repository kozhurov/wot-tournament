package com.tournament.wot.battletime.restore;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.tournament.wot.battletime.core.Constants;
import com.tournament.wot.battletime.core.SharedPref;
import com.tournament.wot.battletime.core.WTApplication;
import com.tournament.wot.battletime.model.alarm.AlarmModel;
import com.tournament.wot.battletime.model.battle.BattleModel;
import com.tournament.wot.battletime.model.players.Friend;
import com.tournament.wot.battletime.model.players.Participant;
import com.tournament.wot.battletime.model.players.Player;
import com.tournament.wot.battletime.model.tanks.BestTanksModel;

import java.sql.SQLException;

public final class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "wot.db";
    private static final int DATABASE_VERSION = 4;

    private RuntimeExceptionDao<Player, Integer> mPlayerDao;
    private RuntimeExceptionDao<Friend, Integer> mFriendDao;
    private RuntimeExceptionDao<BattleModel, Integer> mSimpleBattleDao;
    private RuntimeExceptionDao<Participant, Integer> mParticipantDao;
    private RuntimeExceptionDao<BestTanksModel, Integer> mBestTankDao;
    private RuntimeExceptionDao<AlarmModel, Integer> mAlarmModels;

    private Context mContext;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Player.class);
            TableUtils.createTable(connectionSource, Friend.class);
            TableUtils.createTable(connectionSource, BattleModel.class);
            TableUtils.createTable(connectionSource, Participant.class);
            TableUtils.createTable(connectionSource, BestTanksModel.class);
            TableUtils.createTable(connectionSource, AlarmModel.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int oldVersion, int newVersion) {

        if (oldVersion < DATABASE_VERSION) {
            try {
                TableUtils.dropTable(connectionSource, Player.class, true);
                TableUtils.dropTable(connectionSource, Friend.class, true);
                TableUtils.dropTable(connectionSource, BattleModel.class, true);
                TableUtils.dropTable(connectionSource, Participant.class, true);
                TableUtils.dropTable(connectionSource, BestTanksModel.class, true);
                TableUtils.dropTable(connectionSource, AlarmModel.class, true);

                SharedPref sharedPref = ((WTApplication) mContext.getApplicationContext()).getSharedPref();
                sharedPref.setBaseSyncTimestamp(Constants.ZERO_ARGS);
                sharedPref.setIncrementSyncTimestamp(Constants.ZERO_ARGS);

            } catch (SQLException e) {
                e.printStackTrace();

            }

            onCreate(sqLiteDatabase, connectionSource);
        }

        mContext = null;
    }

    // example of dao
    public RuntimeExceptionDao<Player, Integer> getPlayerDao() {
        if (mPlayerDao == null) {
            mPlayerDao = getRuntimeExceptionDao(Player.class);
        }
        return mPlayerDao;
    }

    public RuntimeExceptionDao<Friend, Integer> getFriendDao() {
        if (mFriendDao == null) {
            mFriendDao = getRuntimeExceptionDao(Friend.class);
        }
        return mFriendDao;
    }

    public RuntimeExceptionDao<BattleModel, Integer> getBattleModelDao() {
        if (mSimpleBattleDao == null) {
            mSimpleBattleDao = getRuntimeExceptionDao(BattleModel.class);
        }
        return mSimpleBattleDao;
    }

    public RuntimeExceptionDao<Participant, Integer> getParticipantDao() {
        if (mParticipantDao == null) {
            mParticipantDao = getRuntimeExceptionDao(Participant.class);
        }
        return mParticipantDao;
    }

    public RuntimeExceptionDao<BestTanksModel, Integer> getBestTankDao() {
        if (mBestTankDao == null) {
            mBestTankDao = getRuntimeExceptionDao(BestTanksModel.class);
        }
        return mBestTankDao;
    }

    public RuntimeExceptionDao<AlarmModel, Integer> getAlarmModels() {
        if (mAlarmModels == null) {
            mAlarmModels = getRuntimeExceptionDao(AlarmModel.class);
        }
        return mAlarmModels;
    }

    @Override
    public void close() {
        super.close();
    }
}
