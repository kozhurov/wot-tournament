package com.tournament.wot.battletime.ui.activitys;

import android.app.ActionBar;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.google.analytics.tracking.android.EasyTracker;
import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.core.SharedPref;
import com.tournament.wot.battletime.core.SupportTaskManager;
import com.tournament.wot.battletime.core.WTApplication;
import com.tournament.wot.battletime.core.bridges.ActivityBridge;
import com.tournament.wot.battletime.core.callbacks.SimpleDialogCallbacks;
import com.tournament.wot.battletime.core.callbacks.SimpleNetCallback;
import com.tournament.wot.battletime.model.battle.BattleModel;
import com.tournament.wot.battletime.model.players.Friend;
import com.tournament.wot.battletime.model.players.Player;
import com.tournament.wot.battletime.net.NetManager;
import com.tournament.wot.battletime.restore.RestoreManager;
import com.tournament.wot.battletime.ui.Launcher;
import com.tournament.wot.battletime.ui.dialogs.AlarmDialog;
import com.tournament.wot.battletime.ui.view.progress.CustomProgressDialog;
import com.tournament.wot.battletime.utils.PushAndNotifyUtils;

import java.util.List;

public final class MainActivity extends Activity implements ActivityBridge {

    private WTApplication mWTApplication;
    private Launcher mFragmentLauncher;
    private DrawerLayout mDrawerLayout;
    private CustomProgressDialog mProgressBar;
    private ActionBarDrawerToggle actionBarDrawerToggle;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Log.e(MainActivity.class.getSimpleName(), "onReceive = " + intent.getExtras());

            String content = intent.getExtras().getString(PushAndNotifyUtils.MESSAGE_KEY);

            if (PushAndNotifyUtils.isEventDelete(content)) {
                RestoreManager restoreManager = getRestoreManager();
                restoreManager.deleteBattleModel(content);
                return;
            }

            if (PushAndNotifyUtils.isAlarm(content)) {
                Log.e(MainActivity.class.getSimpleName(), "alarm");

                long eventId = intent.getExtras().getLong(PushAndNotifyUtils.EVENT_ID_KEY);
                Bundle bundle = AlarmDialog.buildArgs(eventId);
                mFragmentLauncher.launchAlarmDialog(new SimpleDialogCallbacks(), bundle);
                return;
            }

            Log.e(MainActivity.class.getSimpleName(), "begin increment");
            Player player = mWTApplication.getRestoreManager().loadMe();
            int incrementStamp = mWTApplication.getSharedPref().getIncrementTimestamp();
            mWTApplication.getNetManager().incrementSyncProcedure(new NetListener(), player.getId(), incrementStamp);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (getResources().getBoolean(R.bool.is_tab_orientation)) {
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                return;
            }
            initTabMode();

        } else {

            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                return;
            }
            initDrawerMode();
        }

        mWTApplication = (WTApplication) getApplication();

        mFragmentLauncher = new Launcher(getFragmentManager());

        getSupportTaskManager().subscribeToPush(mWTApplication);

        mFragmentLauncher.launchLoginFragment();
        mFragmentLauncher.launchDrawerFragment();
    }

    private void initDrawerMode() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_drawer, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }
        };
        enableBackButton();

        mDrawerLayout.setDrawerListener(actionBarDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (!getResources().getBoolean(R.bool.is_tab_orientation)) {
            actionBarDrawerToggle.syncState();
        }
    }


    private void initTabMode() {
        findViewById(R.id.drawer_fragment_nest).setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (!getResources().getBoolean(R.bool.is_tab_orientation)) {
            if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
                return true;
            }
        }

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
        if (mWTApplication != null) {
            mWTApplication.setIsActivityStarted(true);
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter(MainActivity.class.getSimpleName()));
    }

    @Override
    public NetManager getNetManager() {
        return mWTApplication.getNetManager();
    }

    @Override
    public Launcher getMainLauncher() {
        return mFragmentLauncher;
    }

    @Override
    public SharedPref getSharedPref() {
        return mWTApplication.getSharedPref();
    }

    @Override
    public RestoreManager getRestoreManager() {
        return mWTApplication.getRestoreManager();
    }

    @Override
    public SupportTaskManager getSupportTaskManager() {
        return mWTApplication.getSupportTaskManager();
    }

    @Override
    public void disableDrawer() {
        if (!getResources().getBoolean(R.bool.is_tab_orientation)) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }

    @Override
    public void enableDrawer() {
        if (!getResources().getBoolean(R.bool.is_tab_orientation)) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }

    @Override
    public void closeDrawer() {
        if (!getResources().getBoolean(R.bool.is_tab_orientation) && mDrawerLayout != null) {
            mDrawerLayout.closeDrawers();
        }
    }

    @Override
    public void showProgressBar() {
        if (mProgressBar == null) {
            mProgressBar = new CustomProgressDialog(this);
        }
        mProgressBar.setCancelable(false);
        mProgressBar.show();
    }

    @Override
    public void hideProgressBar() {
        if (mProgressBar != null) {
            mProgressBar.dismiss();
        }
    }

    @Override
    public void disableBackButton() {
        if (getResources().getBoolean(R.bool.is_tab_orientation)) {
            ActionBar actionBar = getActionBar();
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setHomeButtonEnabled(false);
        }else {
            actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        }
    }

    @Override
    public void enableBackButton() {

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        if (getResources().getBoolean(R.bool.is_tab_orientation)) {
//            ActionBar actionBar = getActionBar();
//            actionBar.setDisplayHomeAsUpEnabled(true);
//            actionBar.setHomeButtonEnabled(true);
        }else {
            actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
        }
    }

    @Override
    public void switchToMainTabMode() {
        findViewById(R.id.drawer_fragment_nest).setVisibility(View.VISIBLE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);

        if (mWTApplication != null) {
            mWTApplication.setIsActivityStarted(false);
        }

    }

    private final class NetListener extends SimpleNetCallback {
        @Override
        public void incrementSyncComplete(int pTimestamp, List<BattleModel> pPlatoonBattles, List<Friend> pClanMembers) {
            mWTApplication.getSharedPref().setIncrementSyncTimestamp(pTimestamp);

            RestoreManager restoreManager = mWTApplication.getRestoreManager();
            restoreManager.saveBattleModels(pPlatoonBattles);
            restoreManager.saveClanMembers(pClanMembers);
        }
    }
}
