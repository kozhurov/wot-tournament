package com.tournament.wot.battletime.ui.fragments.base;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.core.SharedPref;
import com.tournament.wot.battletime.core.callbacks.SimpleNetCallback;
import com.tournament.wot.battletime.model.battle.BattleModel;
import com.tournament.wot.battletime.model.players.Friend;
import com.tournament.wot.battletime.restore.RestoreManager;
import com.tournament.wot.battletime.ui.adapters.BattleAdapter;
import com.tournament.wot.battletime.ui.adapters.ClanBattleAdapter;
import com.tournament.wot.battletime.ui.fragments.GenericFragment;
import com.tournament.wot.battletime.utils.SystemUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public abstract class GenericListFragment extends GenericFragment {

    private ListView mBattleList;
    private ArrayAdapter<BattleModel> mBattleListAdapter;
    private SwipeRefreshLayout mPullToRefresh;
    private BattleModel.EventType mEventType;
    private Observer observer = new Observer();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mEventType == BattleModel.EventType.CLAN) {
            mBattleListAdapter = new ClanBattleAdapter(getActivity(), getBattleListByType());
        } else {
            mBattleListAdapter = new BattleAdapter(getActivity(), getBattleListByType());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivityBridge.disableBackButton();

        mActivityBridge.getRestoreManager().addObserver(observer);
        mBattleListAdapter.clear();
        mBattleListAdapter.addAll(getBattleListByType());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_battle_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mBattleList = (ListView) view.findViewById(R.id.simple_battle_list_list);
        mBattleList.setAdapter(mBattleListAdapter);
        mBattleList.setOnItemClickListener(new ListClicker());

        TextView emptyView = (TextView) view.findViewById(R.id.battle_list_empty_view);
        if (mEventType == BattleModel.EventType.CLAN) {
            emptyView.setText(R.string.no_data_in_clan_list);
        } else {
            emptyView.setText(R.string.no_data_in_list);
        }

        mBattleList.setEmptyView(emptyView);

        mActivityBridge.enableDrawer();

        mPullToRefresh = (SwipeRefreshLayout) view.findViewById(R.id.battle_list_pull_to_refresh);
        mPullToRefresh.setColorSchemeResources(R.color.middle_player_color, R.color.ac_player_color, R.color.sky_hight_player_color, R.color.legendary_player_color);
        mPullToRefresh.setOnRefreshListener(new PullToRefresh());

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (mEventType != BattleModel.EventType.CLAN) {
            inflater.inflate(R.menu.simple_plan_list, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_add_new_simple_plan:

                if (SystemUtils.isNetworkConnected(getActivity())) {

                    switch (mEventType) {
                        case PLATOON:
                            mActivityBridge.getMainLauncher().launchCreateSimplePlanFragment();
                            break;

                        case COMPANY:
                            mActivityBridge.getMainLauncher().launchCreateSimpleCompanyFragment();
                            break;

                        case TRAINING:
                            mActivityBridge.getMainLauncher().launchCreateTrainingBattleFragment();
                            break;

                    }

                } else {
                    showNoInternetToast();
                }
                break;
        }


        return false;
    }

    protected void setEventType(BattleModel.EventType pEventType) {
        mEventType = pEventType;
    }

    @Override
    public void onPause() {
        super.onPause();
        mActivityBridge.getRestoreManager().deleteObserver(observer);
    }

    private List<BattleModel> getBattleListByType() {
        List<BattleModel> simpleBattles;
        switch (mEventType) {
            case PLATOON:
                simpleBattles = mActivityBridge.getRestoreManager().getPlatoonsBattle();
                break;

            case COMPANY:
                simpleBattles = mActivityBridge.getRestoreManager().getCompanyBattle();
                break;

            case TRAINING:
                simpleBattles = mActivityBridge.getRestoreManager().getTrainingBattles();
                break;

            case CLAN:
                simpleBattles = mActivityBridge.getRestoreManager().getClansBattle();
                break;

            default:
                simpleBattles = new ArrayList<BattleModel>();
                break;

        }

        return simpleBattles;
    }

    private final class PullToRefresh implements SwipeRefreshLayout.OnRefreshListener {

        @Override
        public void onRefresh() {

            if (!SystemUtils.isNetworkConnected(getActivity())) {
                mPullToRefresh.setRefreshing(false);
                showNoInternetToast();
                return;
            }

            mPullToRefresh.setRefreshing(true);

            SharedPref sharedPref = mActivityBridge.getSharedPref();

            long accountId = mActivityBridge.getRestoreManager().loadMe().getId();
            int timestamp = sharedPref.getIncrementTimestamp();

            mActivityBridge.getNetManager().incrementSyncProcedure(new NetworkCallback(), accountId, timestamp);
        }
    }

    private final class NetworkCallback extends SimpleNetCallback {

        @Override
        public void taskCompleteWithError(final String pErrorCode) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mPullToRefresh.setRefreshing(false);
                    showShortToast(pErrorCode);
                }
            });

        }

        @Override
        public void incrementSyncComplete(final int pTimestamp, List<BattleModel> pSimpleBattles, List<Friend> pClanMembers) {
            mActivityBridge.getSharedPref().setIncrementSyncTimestamp(pTimestamp);
            final RestoreManager restoreManager = mActivityBridge.getRestoreManager();
            restoreManager.saveBattleModels(pSimpleBattles);
            restoreManager.saveClanMembers(pClanMembers);

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mBattleListAdapter.clear();
                    mBattleListAdapter.addAll(getBattleListByType());
                    mPullToRefresh.setRefreshing(false);

                    mActivityBridge.getSharedPref().setIncrementSyncTimestamp(pTimestamp);
                    mBattleListAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    private final class ListClicker implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            BattleModel model = ((BattleModel) parent.getItemAtPosition(position));
            Bundle args = ViewBaseBattleFragment.buildArgs(model.getId());

            if (mEventType == BattleModel.EventType.CLAN && model.isEditable()) {
                mActivityBridge.getMainLauncher().launchCreateClanEventFragment(args);
            } else {
                mActivityBridge.getMainLauncher().launchViewBasicFragment(args);
            }
        }
    }

    private final class Observer implements java.util.Observer {

        @Override
        public void update(Observable observable, Object data) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mBattleListAdapter.clear();
                    mBattleListAdapter.addAll(getBattleListByType());
                }
            });
        }
    }
}
