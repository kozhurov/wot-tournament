package com.tournament.wot.battletime.core.callbacks;

import com.tournament.wot.battletime.model.players.Friend;
import com.tournament.wot.battletime.model.players.Participant;

import org.json.JSONArray;

import java.util.Collection;
import java.util.Date;

public interface DialogsCallbacks {

    public void friendSelected(Collection<Participant> pSelectedFriends);

    public void newCommanderSelected(Friend pNewCommander);

    public void timeSelected(Date pDate, int pTimestamp);
}
