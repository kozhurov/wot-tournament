package com.tournament.wot.battletime.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tournament.wot.battletime.R;

import java.util.List;

public final class DrawerAdapter extends ArrayAdapter<String> {

    private LayoutInflater mLayoutInflater;

    public DrawerAdapter(Context context, List<String> pCategoriesList) {
        super(context, 0, pCategoriesList);
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;

        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.adapter_drawer, parent, false);
            holder = new Holder();

            holder.mCategoryName = (TextView) convertView.findViewById(R.id.drawer_adapter_category_name);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.mCategoryName.setText(getItem(position));

        switch (position) {
            case 0:
                holder.mCategoryName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selector_dawer_platoon, 0, 0, 0);
                break;

            case 1:
                holder.mCategoryName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selector_drawer_company, 0, 0, 0);
                break;

            case 2:
                holder.mCategoryName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selector_dawer_clan, 0, 0, 0);
                break;

            case 3:
                holder.mCategoryName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selector_dawer_training, 0, 0, 0);
                break;

            case 4:
                holder.mCategoryName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selector_drawer_best_tank, 0, 0, 0);
                break;
        }

        return convertView;
    }

    private static final class Holder {
        TextView mCategoryName;
    }
}
