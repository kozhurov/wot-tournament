package com.tournament.wot.battletime.ui.fragments.base;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.mobeta.android.dslv.DragSortController;
import com.mobeta.android.dslv.DragSortListView;
import com.mobeta.android.dslv.DragSortListView.DropListener;
import com.mobeta.android.dslv.DragSortListView.RemoveListener;
import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.core.Constants;
import com.tournament.wot.battletime.core.callbacks.NetCallbacks;
import com.tournament.wot.battletime.core.callbacks.SimpleDialogCallbacks;
import com.tournament.wot.battletime.core.callbacks.SimpleNetCallback;
import com.tournament.wot.battletime.core.receivers.AlarmReceiver;
import com.tournament.wot.battletime.model.battle.BattleModel;
import com.tournament.wot.battletime.model.battle.BattleModel.AnswerType;
import com.tournament.wot.battletime.model.players.Friend;
import com.tournament.wot.battletime.model.players.Participant;
import com.tournament.wot.battletime.model.players.Player;
import com.tournament.wot.battletime.restore.RestoreManager;
import com.tournament.wot.battletime.ui.adapters.ParticipantsAdapter;
import com.tournament.wot.battletime.ui.adapters.comparator.ParticipantComparator;
import com.tournament.wot.battletime.ui.dialogs.FriendDialog;
import com.tournament.wot.battletime.ui.fragments.GenericFragment;
import com.tournament.wot.battletime.ui.view.day_selector.DaySelectorControl;
import com.tournament.wot.battletime.utils.SystemUtils;
import com.tournament.wot.battletime.utils.TimeUtils;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Observable;

public class GenericEventFragment extends GenericFragment {

    protected static final String EVENT_UUID_TRANSPORT_KEY = "event_uuid_transport_key";
    protected DragSortListView mDragSortListView;
    protected EditText mBattleName;
    protected ParticipantsAdapter participantsAdapter;
    protected View mFooterList;
    protected View mUserPicker;

    protected TextView mPlannedStartDateTextView;
    protected TextView mPlannedStartTimeTextView;

    protected DaySelectorControl mDaySelectorControl;
    protected BattleModel mSimpleBattle;
    protected SimpleDialogCallbacks mSimpleDialogCallbacks;
    protected DragSortController mDragSortController;
    protected ImageView mDatePicker;

    private boolean mIsDragEnabled = true;
    private boolean mIsSortEnabled = true;
    private boolean mIsRemoveEnabled = true;
    private Participant selfParticipant;
    private CustomClick mCustomClick;
    private BattleModel.EventType mEventType;
    private int mParticipantSize;
    private int mParticipantNonSpareSize;
    private Player mPlayer;
    private View mButtonSectionView;
    private RadioButton mYesButton, mMayBeButton, mNoButton;
    private boolean mIsCreate = false;
    protected Observer observer;

    private Comparator<Participant> mComparator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        observer = new Observer();

        if (mSimpleBattle == null) {
            mSimpleBattle = new BattleModel();
        }
        mSimpleDialogCallbacks = new CustomDialogCallback();
        mCustomClick = new CustomClick();
        mPlayer = mActivityBridge.getRestoreManager().loadMe();
        mComparator = new ParticipantComparator(mPlayer.getId());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        inflateFooter(R.layout.footer_create_simple_battle, inflater);
        return inflater.inflate(R.layout.fragment_create_simple_battle, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initBasicUI(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivityBridge.disableDrawer();
        mActivityBridge.enableBackButton();
        mActivityBridge.getRestoreManager().addObserver(observer);
    }

    public void setIsDragEnabled(boolean pIsDragEnabled) {
        this.mIsDragEnabled = pIsDragEnabled;
    }

    public void setIsRemoveEnabled(boolean pIsRemoveEnabled) {
        this.mIsRemoveEnabled = pIsRemoveEnabled;
    }

    public void setIsSortEnabled(boolean pIsSortEnabled) {
        this.mIsSortEnabled = pIsSortEnabled;
    }

    @SuppressLint("ClickableViewAccessibility")
    protected void initBasicUI(View view) {
        mDragSortListView = (DragSortListView) view.findViewById(R.id.create_simple_battle_fragment_drop_list);
        mDragSortListView.setEmptyView(new View(getActivity()));
        mDragSortListView.setRemoveListener(new RemoveOurListener());
        mDragSortListView.setDropListener(new DropOurListener());

        mDragSortController = new DragSortController(mDragSortListView);
        mDragSortController.setDragHandleId(R.id.participants_adapter_participant_status);
        mDragSortController.setRemoveEnabled(mIsRemoveEnabled);
        mDragSortController.setSortEnabled(mIsSortEnabled);
        mDragSortController.setDragInitMode(1);

        mDragSortListView.setFloatViewManager(mDragSortController);
        mDragSortListView.setOnTouchListener(mDragSortController);
        mDragSortListView.setDragEnabled(mIsDragEnabled);

        mDragSortListView.addFooterView(mFooterList);

        Collection<Participant> participants = mSimpleBattle.getParticipantsFromBase();

        int adapterMode;
        switch (mEventType){
            case TRAINING:
                adapterMode = ParticipantsAdapter.TRAINING_MODE;
                break;

            default:
                adapterMode = ParticipantsAdapter.OTHERS_MODE;
                break;
        }

        participantsAdapter = new ParticipantsAdapter(getActivity(), participants, adapterMode, mParticipantNonSpareSize);
        if (selfParticipant != null) {
            participantsAdapter.add(selfParticipant);
        }

        participantsAdapter.sort(mComparator);
        mDragSortListView.setAdapter(participantsAdapter);
    }

    protected void inflateFooter(int layoutId, LayoutInflater inflater) {
        mFooterList = inflater.inflate(layoutId, null, false);
        initFooter();
    }

    protected void initFooter() {
        mDatePicker = (ImageView) mFooterList.findViewById(R.id.create_simple_battle_fragment_time_picker);
        mDatePicker.setOnClickListener(mCustomClick);

        mUserPicker = mFooterList.findViewById(R.id.create_simple_battle_fragment_add_user_button);
        mUserPicker.setOnClickListener(mCustomClick);

        mPlannedStartDateTextView = (TextView) mFooterList.findViewById(R.id.create_simple_battle_fragment_start_date);
        mPlannedStartTimeTextView = (TextView) mFooterList.findViewById(R.id.create_simple_battle_fragment_start_time);

        if (!TextUtils.isEmpty(mSimpleBattle.getStartBattleDateString())) {
            parseAndSetDateAndTime(mSimpleBattle.getStartBattleDateString());
        }

        mDaySelectorControl = (DaySelectorControl) mFooterList.findViewById(R.id.create_simple_battle_fragment_repeat_day_selector);

        mBattleName = (EditText) mFooterList.findViewById(R.id.create_simple_battle_fragment_battle_name);

        mButtonSectionView = mFooterList.findViewById(R.id.event_confirm_section);

        mYesButton = (RadioButton) mFooterList.findViewById(R.id.buttonYes);
        if (mYesButton != null) {
            mYesButton.setOnClickListener(mCustomClick);
        }

        mNoButton = (RadioButton) mFooterList.findViewById(R.id.buttonNo);
        if (mNoButton != null) {
            mNoButton.setOnClickListener(mCustomClick);
        }

        mMayBeButton = (RadioButton) mFooterList.findViewById(R.id.buttonMayBe);
        if (mMayBeButton != null) {
            mMayBeButton.setOnClickListener(mCustomClick);
        }

        if (mPlayer.getId() != mSimpleBattle.getAuthorId() && mSimpleBattle.getAuthorId() > 0) {
            if (mButtonSectionView != null) {
                mButtonSectionView.setVisibility(View.VISIBLE);
                for (Participant participant : mSimpleBattle.getParticipantsFromBase()) {
                    if (participant.getParticipantId() == mPlayer.getId()) {
                        switch (participant.getParticipantAnswer()) {
                            case Participant.POSITIVE_ANSWER:
                                if (mYesButton != null) {
                                    mYesButton.setChecked(true);
                                }
                                break;
                            case Participant.MAY_BE_ANSWER:
                                if (mMayBeButton != null) {
                                    mMayBeButton.setChecked(true);
                                }
                                break;
                            case Participant.NEGATIVE_ANSWER:
                                if (mNoButton != null) {
                                    mNoButton.setChecked(true);
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                    }
                }
            }
        } else {
            if (mSimpleBattle.getParticipantsFromBase().size() == 0) {
                selfParticipant = initSelf();
            }
        }

        if (mSimpleBattle.getParticipantsFromBase().size() > 1) {
            mUserPicker.setVisibility(View.GONE);
            mDatePicker.setVisibility(View.GONE);
        }

        if (mEventType == BattleModel.EventType.CLAN) {
            mDatePicker.setVisibility(View.GONE);
            if (mSimpleBattle.isEditable()) {
                mUserPicker.setVisibility(View.VISIBLE);
            } else {
                mUserPicker.setVisibility(View.GONE);
            }
        }

        Log.e("GEF", "is_invited = " + mSimpleBattle.isInvited());
        if (!mSimpleBattle.isInvited()) {
            mYesButton.setVisibility(View.GONE);
            mMayBeButton.setVisibility(View.GONE);
            mNoButton.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(mSimpleBattle.getName())) {
            mBattleName.setText(mSimpleBattle.getName());
            mBattleName.setEnabled(false);
        }

        Collection<Integer> repeats = mSimpleBattle.getRepeatTypeExtras();
        if (repeats != null) {
            mDaySelectorControl.setSelectedDay(repeats);
        }

        // boolean isAuthor = mPlayer.getId() == mSimpleBattle.getAuthorId();
        if (mSimpleBattle.getStartBattleDate() == null) {
            mDaySelectorControl.enableDaySelector();
        } else {
            mDaySelectorControl.disableDaySelector();
        }

        boolean isFutureEvent = TimeUtils.isEventInFuture(mSimpleBattle.getEndBattleDate());
        if (isFutureEvent) {
            mYesButton.setEnabled(true);
            mNoButton.setEnabled(true);
            mMayBeButton.setEnabled(true);
        } else {
            mYesButton.setEnabled(false);
            mNoButton.setEnabled(false);
            mMayBeButton.setEnabled(false);
        }

    }

    private Participant initSelf() {
        Participant self;
        self = new Participant(0, getResources().getString(R.string.self_name));
        self.setParticipantAnswer(Participant.POSITIVE_ANSWER);
        self.setWinRate(mPlayer.getWinRate());
        self.setDamageRate(mPlayer.getDamageRate());
        self.setParticipantRole(0);

        return self;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (!SystemUtils.isNetworkConnected(getActivity().getApplication())) {
            showNoInternetToast();
            return super.onOptionsItemSelected(item);
        }

        switch (item.getItemId()) {
            case R.id.menu_save_simple_plan:

                if (participantsAdapter.getCount() == 1) {
                    showShortToast(R.string.empty_event_participant_error);
                    return super.onOptionsItemSelected(item);
                }

                if (TextUtils.isEmpty(mBattleName.getText().toString())) {
                    showShortToast(R.string.empty_event_name_error);
                    return super.onOptionsItemSelected(item);
                }

                SystemUtils.hideKeyboard(getActivity(), mBattleName);

                mActivityBridge.showProgressBar();
                prepareSimpleModel();
                NetCallbacks netCallbacks = new NetworkCallback();

                if (mEventType == BattleModel.EventType.CLAN) {
                    mActivityBridge.getNetManager().updateBattleModel(netCallbacks, mSimpleBattle, mPlayer.getId());
                } else {
                    mActivityBridge.getNetManager().createBattleModel(netCallbacks, mSimpleBattle);
                }

                break;

            case R.id.menu_delete_simple_item:

                if (mSimpleBattle.getAuthorId() != mPlayer.getId()) {
                    showShortToast(R.string.author_permission_delete_error);
                    return super.onOptionsItemSelected(item);
                }

                mActivityBridge.showProgressBar();
                mActivityBridge.getNetManager().deleteEvent(new NetworkCallback(), mSimpleBattle.getUUID(), Constants.WRONG_ARGS);
                break;

            case R.id.menu_refresh_simple_plan:
                //TODO Need add rotate animation
                Player player = mActivityBridge.getRestoreManager().loadMe();
                long accountId = player.getId();
                int incrementTimestamp = mActivityBridge.getSharedPref().getIncrementTimestamp();
                mActivityBridge.getNetManager().incrementSyncProcedure(new NetworkCallback(), accountId, incrementTimestamp);
                break;

            case R.id.menu_re_send_notify_simple_plan:

                if (mSimpleBattle.getAuthorId() != mPlayer.getId()) {
                    showShortToast(R.string.author_permission_re_send_error);
                    return super.onOptionsItemSelected(item);
                }

                if (TimeUtils.isAcceptResend(mActivityBridge.getSharedPref().getLastReSyncTimestamp())) {
                    mActivityBridge.getNetManager().reSendInvites(new NetworkCallback(), mSimpleBattle.getUUID());
                    mActivityBridge.getSharedPref().setLastReSyncTimestamp(System.currentTimeMillis());
                } else {
                    showShortToast(R.string.too_many_re_send_error);
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void prepareSimpleModel() {
        mSimpleBattle.getParticipants().remove(selfParticipant);

        mSimpleBattle.setBattleName(mBattleName.getText().toString());
        mSimpleBattle.setAuthorId(mPlayer.getId());
        mSimpleBattle.setEventType(mEventType.ordinal());
        mSimpleBattle.setRepeatTypeExtras(mDaySelectorControl.getSelectedDay());

        if (mSimpleBattle.getStartBattleDate() == null) {
            mSimpleBattle.setStartBattleTime(TimeUtils.setDefaultStartDate());
            mSimpleBattle.setEndBattleTime(TimeUtils.setDefaultEndDate());
        }
    }

    protected void dropMethod(int from, int to) {
        Participant participant = participantsAdapter.getItem(from);
        participantsAdapter.remove(participant);
        participantsAdapter.insert(participant, to);
    }

    protected void removeMethod(int which) {
        mUserPicker.setVisibility(View.VISIBLE);
        participantsAdapter.remove(participantsAdapter.getItem(which));
    }

    protected void setEventType(BattleModel.EventType eventType) {
        mEventType = eventType;
        switch (eventType) {
            case PLATOON:
                mParticipantSize = 3;
                mParticipantNonSpareSize = 3;
                break;

            case CLAN:
                mParticipantSize = 19;
                mParticipantNonSpareSize = 15;
                break;

            case COMPANY:
                mParticipantSize = 19;
                mParticipantNonSpareSize = 15;
                break;

            case TRAINING:
                mParticipantSize = 39;
                mParticipantNonSpareSize = 30;
                break;

            default:
                break;
        }
    }

    protected void setEventType(int eventType) {
        setEventType(BattleModel.EventType.values()[eventType]);
    }

    private void parseAndSetDateAndTime(String pStartDate) {
        //todo givno need split for body " " ?
        int firstSplitPoint = pStartDate.length() - 7;
        int secondSplitPoint = pStartDate.length() - 5;

        mPlannedStartDateTextView.setText(getString(R.string.create_event_start_data) + Constants.MIDDLE_LITERAL + pStartDate.substring(0, firstSplitPoint));
        mPlannedStartTimeTextView.setText(getString(R.string.create_event_start_time) + Constants.MIDDLE_LITERAL + pStartDate.substring(secondSplitPoint));
    }

    @Override
    public void onPause() {
        super.onPause();
        mActivityBridge.getRestoreManager().deleteObserver(observer);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSimpleBattle = null;
        mSimpleDialogCallbacks = null;
        mCustomClick = null;
    }

    private final class DropOurListener implements DropListener {
        @Override
        public void drop(int from, int to) {
            dropMethod(from, to);
        }
    }

    private final class RemoveOurListener implements RemoveListener {
        @Override
        public void remove(int which) {
            removeMethod(which);
        }
    }

    private final class NetworkCallback extends SimpleNetCallback {

        @Override
        public void battleCreated(final BattleModel pSimpleBattle) {
            mActivityBridge.getRestoreManager().saveBattleModel(mSimpleBattle, true);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {

                    long time = pSimpleBattle.getStartBattleDate().getTime();
                    long eventUUID = pSimpleBattle.getId();

                    Log.e("battleCreated", pSimpleBattle.getStartBattleDate().toGMTString());

                    AlarmReceiver.setAlarm(getActivity().getApplicationContext(), time, eventUUID, mDaySelectorControl.getSelectedDay());

//                    LocalAlarmManager localAlarmManager = new LocalAlarmManager(mActivityBridge.getRestoreManager());
//                    localAlarmManager.setAlarmForEvent(mSimpleBattle, getActivity());
                    mIsCreate = true;
                }
            });
        }

        @Override
        public void userAnswerAccepted(int pTimestamp, int pAnswerFlag) {
            RestoreManager restoreManager = mActivityBridge.getRestoreManager();
            long userId = restoreManager.loadMe().getId();
            Collection<Participant> participants = mSimpleBattle.getParticipantsFromBase();

            for (Participant participant : participants) {
                if (participant.getParticipantId() == userId) {
                    participant.setParticipantAnswer(pAnswerFlag);
                    restoreManager.saveParticipant(participant);
                    break;
                }
            }

            mSimpleBattle.setUserAnswer(pAnswerFlag);
            restoreManager.updateBattleModel(mSimpleBattle);

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mActivityBridge.hideProgressBar();
                    participantsAdapter.notifyDataSetChanged();
                }
            });
        }

        @Override
        public void reSendComplete() {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    showShortToast(R.string.complete_re_send_invite);
                }
            });
        }

        @Override
        public void taskCompleteWithError(final String pErrorCode) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mActivityBridge.hideProgressBar();
                    showShortToast(pErrorCode);
                }
            });
        }

        @Override
        public void deleteEventComplete() {
            mActivityBridge.getRestoreManager().deleteBattleModel(mSimpleBattle);

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mActivityBridge.hideProgressBar();
                    getActivity().onBackPressed();
                }
            });
        }

        @Override
        public void incrementSyncComplete(int pTimestamp, List<BattleModel> pBattles, List<Friend> pClanMembers) {
            mActivityBridge.getSharedPref().setIncrementSyncTimestamp(pTimestamp);
            final RestoreManager restoreManager = mActivityBridge.getRestoreManager();
            restoreManager.saveBattleModels(pBattles);
            restoreManager.saveClanMembers(pClanMembers);
        }

        @Override
        public void battleUpdated(BattleModel pBattleModel) {
            mActivityBridge.getRestoreManager().saveBattleModel(pBattleModel, true);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mActivityBridge.hideProgressBar();
                    getActivity().onBackPressed();
                }
            });
        }
    }

    private final class CustomDialogCallback extends SimpleDialogCallbacks {
        @Override
        public void friendSelected(Collection<Participant> pSelectedFriends) {

            if (mParticipantSize == pSelectedFriends.size()) {
                mUserPicker.setVisibility(View.GONE);
            } else {
                if (mUserPicker.getVisibility() == View.GONE) {
                    mUserPicker.setVisibility(View.VISIBLE);
                }
            }
            mSimpleBattle.setBattleParticipants(pSelectedFriends);
            participantsAdapter.clear();

            if (selfParticipant != null) {
                pSelectedFriends.add(selfParticipant);
            } else {
                pSelectedFriends.add(initSelf());
            }

            participantsAdapter.addAll(pSelectedFriends);
            participantsAdapter.notifyDataSetChanged();
            participantsAdapter.sort(mComparator);

        }

        @Override
        public void timeSelected(Date pDate, int pTimestamp) {
            parseAndSetDateAndTime(DateFormat.format(Constants.DD_MM_YYYY_HH_MM_DATE_FORMAT, pDate).toString());
            mSimpleBattle.setStartBattleTime(pDate);
        }
    }

    private final class CustomClick implements View.OnClickListener {
        @Override
        public void onClick(View v) {

            if (!SystemUtils.isNetworkConnected(getActivity().getApplication())) {
                showNoInternetToast();
                return;
            }

            int answerType = Constants.WRONG_ARGS;
            switch (v.getId()) {
                case R.id.create_simple_battle_fragment_time_picker:
                    mActivityBridge.getMainLauncher().launchTimeDialog(mSimpleDialogCallbacks);
                    break;

                case R.id.create_simple_battle_fragment_add_user_button:

                    Bundle args;
                    switch (mEventType) {
//                        case TRAINING:
//                            args = FriendDialog.buildArgs(mParticipantSize, mParticipantNonSpareSize, FriendDialog.MODE_FRIENDS);
//                            break;

                        case CLAN:
                            args = FriendDialog.buildArgs(mParticipantSize, mParticipantNonSpareSize, FriendDialog.MODE_CLAN_MEMBER);
                            break;

                        default:
                            args = FriendDialog.buildArgs(mParticipantSize, mParticipantNonSpareSize, FriendDialog.MODE_FRIENDS);
                            break;
                    }
                    mActivityBridge.getMainLauncher().launchFriendsDialog(mSimpleDialogCallbacks, args);
                    break;

                case R.id.buttonYes:
                    answerType = AnswerType.YES.ordinal();
                    break;

                case R.id.buttonNo:
                    answerType = AnswerType.NO.ordinal();
                    break;

                case R.id.buttonMayBe:
                    answerType = AnswerType.MAYBE.ordinal();
                    break;
            }

            if (answerType != Constants.WRONG_ARGS) {
                mActivityBridge.showProgressBar();
                mActivityBridge.getNetManager().userAnswer(new NetworkCallback(), mPlayer.getId(), mSimpleBattle.getUUID(), answerType);
            }
        }
    }

    private final class Observer implements java.util.Observer {

        @Override
        public void update(Observable observable, Object data) {
            if (participantsAdapter != null) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {

                        long id = mSimpleBattle.getId();
                        mSimpleBattle = mActivityBridge.getRestoreManager().getBattleModel(id);

                        participantsAdapter.clear();
                        participantsAdapter.addAll(mSimpleBattle.getParticipantsFromBase());

                        if (mIsCreate) {
                            mActivityBridge.hideProgressBar();
                            getActivity().onBackPressed();
                        }
                    }
                });
            }
        }
    }
}
