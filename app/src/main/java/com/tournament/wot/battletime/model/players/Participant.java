package com.tournament.wot.battletime.model.players;

import android.util.Log;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tournament.wot.battletime.core.Constants;
import com.tournament.wot.battletime.model.battle.BattleModel;

import org.json.JSONException;
import org.json.JSONObject;

@DatabaseTable(tableName = "participant")
public class Participant {
    public static final int MAIN_PARTICIPANT = 0;
    public static final int RESERVE_PARTICIPANT = 1;
    public static final int MAIN_SECONDARY_TEAM_PARTICIPANT = 2;
    public static final int RESERVE_SECONDARY_TEAM_PARTICIPANT = 3;

    public static final int OUT_OF_SERVICE = -1;
    public static final int POSITIVE_ANSWER = 0;
    public static final int NEGATIVE_ANSWER = 1;
    public static final int MAY_BE_ANSWER = 2;
    public static final int NON_ANSWER = 3;

    private static final String PARTICIPANT_ID = "ParticipantId";
    private static final String PARTICIPANT_ROLE = "Role";
    private static final String ANSWER_TIME = "AnswerTime";

    private static final String NAME = "Name";
    private static final String PLAYER_ROLE = "PlayerRole";
    private static final String USER_ANSWER = "UserAnswer";
    private static final String USER_ID = "UserId";
    private static final String DAMAGE_RATE = "AvgDamage";
    private static final String WIN_RATE = "WinRate";

    @DatabaseField(columnName = "participant_answer", defaultValue = "-1")
    private int mParticipantAnswer = NON_ANSWER;

    @DatabaseField(columnName = "win_rate")
    private int mWinRate;

    @DatabaseField(columnName = "participant_id")
    private long mParticipantId;

    @DatabaseField(generatedId = true)
    private int mId;

    @DatabaseField(columnName = "participant_role")
    private int mParticipantRole = Constants.WRONG_ARGS;

    @DatabaseField(columnName = "participant_name")
    private String mParticipantName;

    @DatabaseField(columnName = "participant_answer_time")
    private int mParticipantAnswerTime;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, foreignAutoCreate = true, columnName = "simple_battles_id")
    private BattleModel mBattleModel;

    @DatabaseField(columnName = "damage_rate")
    private int mDamageRate;

    private int mPrevioseRole;

    public Participant() {

    }

    public Participant(JSONObject pParticipantJSONObject) throws JSONException {
        mParticipantId = pParticipantJSONObject.getLong(USER_ID);
        mParticipantRole = pParticipantJSONObject.optInt(PLAYER_ROLE, Constants.WRONG_ARGS);
        mParticipantAnswer = pParticipantJSONObject.optInt(USER_ANSWER, OUT_OF_SERVICE);

        mParticipantName = pParticipantJSONObject.optString(NAME);
        mParticipantAnswerTime = pParticipantJSONObject.optInt(ANSWER_TIME, Constants.WRONG_ARGS);

        mDamageRate = pParticipantJSONObject.optInt(DAMAGE_RATE, Constants.ZERO_ARGS);
        mWinRate = pParticipantJSONObject.optInt(WIN_RATE, Constants.ZERO_ARGS);
    }

    public Participant(long pParticipantId, String pParticipantName) {
        mParticipantId = pParticipantId;
        mParticipantName = pParticipantName;
    }

    public int getParticipantRole() {
        return mParticipantRole;
    }

    public void setParticipantRole(int pParticipantRole) {
        mParticipantRole = pParticipantRole;
    }

    public long getParticipantId() {
        return mParticipantId;
    }

    public void setParticipantId(long pParticipantId) {
        mParticipantId = pParticipantId;
    }

    public JSONObject getConvertedJsonObject() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(PARTICIPANT_ID, mParticipantId);
        jsonObject.put(PARTICIPANT_ROLE, mParticipantRole);

        return jsonObject;
    }

    public String getParticipantName() {
        return mParticipantName;
    }

    public void setParticipantName(String pParticipantName) {
        mParticipantName = pParticipantName;
    }

    public int getParticipantAnswer() {
        return mParticipantAnswer;
    }

    public void setParticipantAnswer(int pParticipantAnswer) {
        mParticipantAnswer = pParticipantAnswer;
    }

    public int getWinRate() {
        return mWinRate;
    }

    public int getDamageRate() {
        return mDamageRate;
    }

    public void setBattleModel(BattleModel pBattleModel) {
        mBattleModel = pBattleModel;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public void setWinRate(int pWinRate) {
        mWinRate = pWinRate;
    }

    public void setDamageRate(int pDamageRate) {
        mDamageRate = pDamageRate;
    }

    public void setPrevioseRole(int mPrevioseRole) {
        this.mPrevioseRole = mPrevioseRole;
    }

    public int getPrevioseRole() {
        return mPrevioseRole;
    }
}