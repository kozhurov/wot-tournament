package com.tournament.wot.battletime.core;

import android.content.Context;
import android.content.SharedPreferences;

public final class SharedPref {

    private static final String NAME = "WoTTournament";
    private static final String ACCESS_TOKEN = "access_token";
    private static final String TOKEN_DIE_TIME = "token_die_time";
    private static final String BASE_SYNC_TIMESTAMP = "base_sync_timestamp";
    private static final String INCREMENT_SYNC_TIMESTAMP = "increment_sync_timestamp";
    private static final String LAST_RE_SYNC_TIMESTAMP = "last_re_sync_timestamp";
    private static final String DEVICE_ID = "device_id";

    private final SharedPreferences mSharedPreferences;

    public SharedPref(Context pContext) {
        mSharedPreferences = pContext.getSharedPreferences(NAME, Context.MODE_PRIVATE);
    }

    public void setAuthToken(String pAuthToken) {
        mSharedPreferences.edit().putString(ACCESS_TOKEN, pAuthToken).commit();
    }

    public String getAccessToken() {
        return mSharedPreferences.getString(ACCESS_TOKEN, Constants.EMPTY_STRING);
    }

    public int getTokenDieTime() {
        return mSharedPreferences.getInt(TOKEN_DIE_TIME, Constants.WRONG_ARGS);
    }

    public void setTokenDieTime(int pTokenDieTime) {
        mSharedPreferences.edit().putInt(TOKEN_DIE_TIME, pTokenDieTime).commit();
    }

    public int getBaseSyncTimestamp() {
        return mSharedPreferences.getInt(BASE_SYNC_TIMESTAMP, Constants.ZERO_ARGS);
    }

    public void setBaseSyncTimestamp(int pSyncTimestamp) {
        mSharedPreferences.edit().putInt(BASE_SYNC_TIMESTAMP, pSyncTimestamp).commit();
    }

    public String getDeviceId() {
        return mSharedPreferences.getString(DEVICE_ID, Constants.EMPTY_STRING);
    }

    public void setDeviceId(String pDeviceId) {
        mSharedPreferences.edit().putString(DEVICE_ID, pDeviceId).commit();
    }

    public int getIncrementTimestamp() {
        return mSharedPreferences.getInt(INCREMENT_SYNC_TIMESTAMP, Constants.ZERO_ARGS);
    }

    public void setIncrementSyncTimestamp(int pSyncTimestamp) {
        mSharedPreferences.edit().putInt(INCREMENT_SYNC_TIMESTAMP, pSyncTimestamp).commit();
    }

    public long getLastReSyncTimestamp() {
        return mSharedPreferences.getLong(LAST_RE_SYNC_TIMESTAMP, Constants.ZERO_ARGS);
    }

    public void setLastReSyncTimestamp(long pReSyncTimestamp) {
        mSharedPreferences.edit().putLong(LAST_RE_SYNC_TIMESTAMP, pReSyncTimestamp).commit();
    }

}
