package com.tournament.wot.battletime.ui.fragments.battle.clan;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.core.Constants;
import com.tournament.wot.battletime.core.callbacks.SimpleDialogCallbacks;
import com.tournament.wot.battletime.core.callbacks.SimpleNetCallback;
import com.tournament.wot.battletime.model.battle.BattleModel.EventType;
import com.tournament.wot.battletime.model.players.Friend;
import com.tournament.wot.battletime.ui.dialogs.FriendDialog;
import com.tournament.wot.battletime.ui.fragments.base.GenericEventFragment;

public class CreateClanBattleFragment extends GenericEventFragment {

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        long eventUUID = getArguments().getLong(EVENT_UUID_TRANSPORT_KEY, Constants.WRONG_ARGS);
        mSimpleBattle = mActivityBridge.getRestoreManager().getBattleModel(eventUUID);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
        setEventType(EventType.CLAN);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.edit_clan_battle, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_change_commander:

                if (mSimpleBattle.isEditable()) {
                    Bundle args = FriendDialog.buildArgs(1, 1, FriendDialog.MODE_COMMANDER);
                    mActivityBridge.getMainLauncher().launchFriendsDialog(new DialogListener(), args);
                } else {
                    showShortToast(R.string.author_permission_change_commander);
                }
                break;

            default:
                super.onOptionsItemSelected(item);

                break;
        }

        return false;
    }

    private void beginChangeCommanderProcedure(Friend pNewCommanderId) {
        mActivityBridge.showProgressBar();
        mActivityBridge.getNetManager().changeOwner(new NetListener(), mSimpleBattle.getUUID(), pNewCommanderId.getId());
    }

    private final class DialogListener extends SimpleDialogCallbacks {

        @Override
        public void newCommanderSelected(Friend pNewCommander) {
            if (pNewCommander != null) {
                beginChangeCommanderProcedure(pNewCommander);
            }
        }
    }

    private final class NetListener extends SimpleNetCallback {
        @Override
        public void taskCompleteWithError(final String pErrorCode) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mActivityBridge.hideProgressBar();
                    showShortToast(pErrorCode);
                }
            });
        }

        @Override
        public void changeCommanderComplete(long pNewCommanderId, String pEventUUID) {
            mActivityBridge.getRestoreManager().deleteObserver(observer);
            mActivityBridge.getRestoreManager().deleteBattleModel(pEventUUID);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mActivityBridge.hideProgressBar();
                    getActivity().onBackPressed();
                }
            });
        }
    }
}
