package com.tournament.wot.battletime.model.players;

import android.text.format.DateFormat;

import com.j256.ormlite.field.DatabaseField;
import com.tournament.wot.battletime.core.Constants;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class GenericPlayer {

    private static final String JSON_USER_NAME = "Name";
    private static final String JSON_LAST_BATTLE_TIME = "LastBattleTime";

    static final String DAMAGE_RATE = "AvgDamage";
    static final String WIN_RATE = "WinRate";

    @DatabaseField(id = true)
    protected long mId;

    @DatabaseField(columnName = "name")
    protected String mName;

    @DatabaseField(columnName = "last_battle_time")
    protected int mLastBattleTime;

    @DatabaseField(columnName = "last_battle_date")
    protected String mLastBattleDate;

    @DatabaseField(columnName = "damage_rate")
    protected int mDamageRate;

    @DatabaseField(columnName = "win_rate")
    protected int mWinRate;

    protected GenericPlayer() {

    }

    protected GenericPlayer(JSONObject pJSONObject) throws JSONException {

        mName = pJSONObject.getString(JSON_USER_NAME);
        mLastBattleTime = pJSONObject.getInt(JSON_LAST_BATTLE_TIME);

        long extendedStamp = mLastBattleTime;
        long bigTimestamp = extendedStamp * 1000;

        mLastBattleDate = (String) DateFormat.format(Constants.DD_MM_YYYY_HH_MM_DATE_FORMAT, bigTimestamp);
        mDamageRate = pJSONObject.optInt(DAMAGE_RATE, Constants.ZERO_ARGS);
        mWinRate = pJSONObject.optInt(WIN_RATE, Constants.ZERO_ARGS);
    }

    public String getName() {
        return mName;
    }

    public int getLastBattleTime() {
        return mLastBattleTime;
    }

    public String getLastBattleDate() {
        return mLastBattleDate;
    }

    public long getId() {
        return mId;
    }

    public int getDamageRate() {
        return mDamageRate;
    }

    public int getWinRate() {
        return mWinRate;
    }

}
