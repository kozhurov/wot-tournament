package com.tournament.wot.battletime.ui.activitys;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.core.SharedPref;
import com.tournament.wot.battletime.ui.view.progress.CustomProgressDialog;

public final class AuthConfirmActivity extends Activity {

    public static final int THIS_ACTIVITY_START_KEY = 100;
    private static final String AUTH_REDIRECT = "auth_redirect";
    private static final String ACCESS_TOKEN_KEY = "access_token";
    private static final String ACCOUNT_ID = "account_id";
    private static final String TOKEN_DIE_TIME = "expires_at";
    private static final String RESPONSE_SPLIT_KEY = "&";
    private static final String VALUE_SPLIT_KEY = "=";
    private WebView mWebView;
    private CustomProgressDialog mProgressBar;

    public static Intent buildIntent(Context pContext, String pAuthRedirect) {
        Intent intent = new Intent(pContext, AuthConfirmActivity.class);

        Bundle bundle = new Bundle();
        bundle.putString(AUTH_REDIRECT, pAuthRedirect);

        intent.putExtras(bundle);
        return intent;
    }

    public static long parseAndSaveResult(Intent pIntent, SharedPref pSharedPref) {
        Bundle bundle = pIntent.getExtras();
        pSharedPref.setAuthToken(bundle.getString(ACCESS_TOKEN_KEY));
        pSharedPref.setTokenDieTime(bundle.getInt(TOKEN_DIE_TIME));

        return bundle.getLong(ACCOUNT_ID);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth_confirm);

        mProgressBar = new CustomProgressDialog(this);
        mProgressBar.show();

        mWebView = (WebView) findViewById(R.id.auth_web_view);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new WebClient());

        String redirectUrl = getIntent().getExtras().getString(AUTH_REDIRECT);
        mWebView.loadUrl(redirectUrl);
    }

    private final class WebClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            if (url.contains(ACCESS_TOKEN_KEY)) {
                Intent intent = new Intent();
                intent.putExtras(parseResult(url));
                setResult(RESULT_OK, intent);
                AuthConfirmActivity.this.finish();
            } else {
                mProgressBar.dismiss();
            }

            return false;
        }

        private Bundle parseResult(String pUrl) {
            String[] parts = pUrl.split(RESPONSE_SPLIT_KEY);
            Bundle bundle = new Bundle();
            for (String part : parts) {

                if (part.contains(ACCESS_TOKEN_KEY)) {
                    bundle.putString(ACCESS_TOKEN_KEY, part.split(VALUE_SPLIT_KEY)[1]);
                    continue;
                }

                if (part.contains(ACCOUNT_ID)) {
                    bundle.putLong(ACCOUNT_ID, Long.valueOf(part.split(VALUE_SPLIT_KEY)[1]));
                    continue;
                }

                if (part.contains(TOKEN_DIE_TIME)) {
                    bundle.putInt(TOKEN_DIE_TIME, Integer.parseInt(part.split(VALUE_SPLIT_KEY)[1]));
                    continue;
                }
            }

            return bundle;
        }
    }
}
