package com.tournament.wot.battletime.ui.fragments.tank;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.core.callbacks.NetCallbacks;
import com.tournament.wot.battletime.core.callbacks.SimpleNetCallback;
import com.tournament.wot.battletime.model.tanks.BestTanksModel;
import com.tournament.wot.battletime.ui.adapters.BestTankAdapter;
import com.tournament.wot.battletime.ui.fragments.GenericFragment;
import com.tournament.wot.battletime.utils.SystemUtils;

import java.util.List;

public final class BestTankFragment extends GenericFragment {

    private ListView mTanksListView;
    private BestTankAdapter mBestTankAdapter;
    private SwipeRefreshLayout mPullToRefresh;

    private NetCallbacks networkCallback = new SimpleNetCallback() {

        public void loadBestTanks(final java.util.ArrayList<BestTanksModel> pBestTanksModel) {
            final boolean isFeel = pBestTanksModel.size() > 0;
            if (isFeel) {
                mActivityBridge.getRestoreManager().saveBestTanks(pBestTanksModel);
            }

            if (isResumed()) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        mPullToRefresh.setRefreshing(false);
                        mBestTankAdapter.clear();
                        mBestTankAdapter.addAll(pBestTanksModel);
                        mBestTankAdapter.notifyDataSetChanged();
                    }
                });
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<BestTanksModel> tanksModels = mActivityBridge.getRestoreManager().getBestTanks();
        mBestTankAdapter = new BestTankAdapter(getActivity(), tanksModels);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_battle_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        TextView emptyView = (TextView) view.findViewById(R.id.battle_list_empty_view);
        emptyView.setText(Html.fromHtml(getString(R.string.no_best_tank_data_error)));

        mTanksListView = (ListView) view.findViewById(R.id.simple_battle_list_list);
        mTanksListView.setEmptyView(emptyView);
        mTanksListView.setAdapter(mBestTankAdapter);

        mPullToRefresh = (SwipeRefreshLayout) view.findViewById(R.id.battle_list_pull_to_refresh);
        mPullToRefresh.setColorSchemeResources(R.color.middle_player_color, R.color.ac_player_color, R.color.sky_hight_player_color, R.color.legendary_player_color);
        mPullToRefresh.setOnRefreshListener(new PullToRefresh());
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivityBridge.enableDrawer();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPullToRefresh.setRefreshing(false);
    }

    private final class PullToRefresh implements SwipeRefreshLayout.OnRefreshListener {

        @Override
        public void onRefresh() {
            if (!SystemUtils.isNetworkConnected(getActivity().getApplication())) {
                showNoInternetToast();
                mPullToRefresh.setRefreshing(false);
                return;
            }

            mPullToRefresh.setRefreshing(true);
            mActivityBridge.getNetManager().loadBestTanks(networkCallback, mActivityBridge.getRestoreManager().loadMe().getId(), SystemUtils.localeNormalize());
        }
    }
}
