package com.tournament.wot.battletime.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.model.battle.BattleModel;
import com.tournament.wot.battletime.model.players.Participant;

import java.util.List;

public final class BattleAdapter extends ArrayAdapter<BattleModel> {

    private final LayoutInflater mLayoutInflater;

    public BattleAdapter(Context context, List<BattleModel> pBattleList) {
        super(context, 0, pBattleList);
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BattleModel battle = getItem(position);
        Holder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.adapter_simple_battle, parent, false);
            holder = new Holder();

            holder.mBattleStartTime = (TextView) convertView.findViewById(R.id.simple_battle_adapter_battle_start_time);
            holder.mBattleName = (TextView) convertView.findViewById(R.id.simple_battle_adapter_battle_name);
            holder.mUserAnswer = (ImageView) convertView.findViewById(R.id.simple_battle_adapter_user_answer);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.mBattleName.setText(battle.getName());
        holder.mBattleStartTime.setText(battle.getStartBattleDateString());

        int resId;
        switch (battle.getUserAnswer()) {
            case Participant.NEGATIVE_ANSWER:
                resId = R.drawable.ic_reject;
                break;

            case Participant.POSITIVE_ANSWER:
                resId = R.drawable.ic_check;
                break;

            case Participant.MAY_BE_ANSWER:
                resId = R.drawable.ic_may_be;
                break;

            default:
                resId = R.drawable.ic_non_answer;
                break;

        }

        holder.mUserAnswer.setImageResource(resId);

        long startTime = battle.getStartBattleDate().getTime();
        long endTime = battle.getEndBattleDate().getTime();
        long currentTime = System.currentTimeMillis();

        if (currentTime > endTime) {
            convertView.setBackgroundResource(R.color.past_event);
            return convertView;
        }

        if (currentTime < startTime) {
            convertView.setBackgroundResource(R.color.future_event);
            return convertView;
        }

        convertView.setBackgroundResource(R.color.current_event);
        return convertView;
    }

    private static final class Holder {
        TextView mBattleName;
        TextView mBattleStartTime;
        ImageView mUserAnswer;
    }
}
