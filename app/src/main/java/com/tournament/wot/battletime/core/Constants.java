package com.tournament.wot.battletime.core;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

public final class Constants {

    public static final String GOOGLE_SENDER_ID = "105579749297";

    public static final int ZERO_ARGS = 0;
    public static final int WRONG_ARGS = -1;
    public static final int ONE_NUMBER_KEY = 1;
    public static final String EMPTY_STRING = "";
    public static final String MIDDLE_LITERAL = " : ";

    public static final String DD_MM_YYYY_HH_MM_DATE_FORMAT = "dd.MM.yyyy, kk:mm";
    public static final String DD_MM_DATE_FORMAT = "dd:MM";
    public static final String YYYY_MM_DD_HH_MM_DATE_FORMAT = "yyyy/MM/dd kk:mm";
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(YYYY_MM_DD_HH_MM_DATE_FORMAT);

    public static SimpleDateFormat getSimpleDateFormat() {
        SIMPLE_DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("GMT"));
        return SIMPLE_DATE_FORMAT;
    }

    public enum TankType {
        AT, LT, MT, HT, ATT
    }
}
