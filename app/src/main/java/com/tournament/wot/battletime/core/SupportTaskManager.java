package com.tournament.wot.battletime.core;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.util.concurrent.Executor;

public final class SupportTaskManager {

    private final Executor mExecutor;

    public SupportTaskManager(Executor pExecutor) {
        mExecutor = pExecutor;
    }

    public void subscribeToPush(final WTApplication pWTApplication) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {

                GoogleCloudMessaging cloudMessaging = GoogleCloudMessaging.getInstance(pWTApplication.getApplicationContext());
                String regId = null;
                try {
                    regId = cloudMessaging.register(Constants.GOOGLE_SENDER_ID);
                    pWTApplication.getSharedPref().setDeviceId(regId);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }
}
