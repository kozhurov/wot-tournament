package com.tournament.wot.battletime.model.players;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.json.JSONException;
import org.json.JSONObject;

@DatabaseTable(tableName = "friend")
public final class Friend extends GenericPlayer {

    private static final String JSON_USER_ID = "UserId";
    private static final String JSON_IS_USER_REGISTERED = "IsRegistered";

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "player_id")
    private Player player;

    @DatabaseField(columnName = "is_friend")
    private boolean isFriend;

    @DatabaseField(columnName = "is_clan")
    private boolean isClanMember;

    @DatabaseField(columnName = "is_registered")
    private boolean isRegistered;


    public Friend() {

    }

    public Friend(long id) {
        mId = id;
    }

    public Friend(JSONObject pJSONObject, boolean pIsFriend) throws JSONException {
        super(pJSONObject);
        mId = pJSONObject.getLong(JSON_USER_ID);
        isFriend = pIsFriend;
        isRegistered = pJSONObject.getBoolean(JSON_IS_USER_REGISTERED);
    }

    public Friend(JSONObject pJSONObject, boolean pIsClan, Object fakeObj) throws JSONException {
        super(pJSONObject);
        mId = pJSONObject.getLong(JSON_USER_ID);
        isClanMember = pIsClan;
        isRegistered = pJSONObject.getBoolean(JSON_IS_USER_REGISTERED);
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public boolean isFriend() {
        return isFriend;
    }

    public boolean isRegistered() {
        return isRegistered;
    }

    public boolean isClanMember() {
        return isClanMember;
    }

    public void setClanMember(boolean isClanMember) {
        this.isClanMember = isClanMember;
    }
}
