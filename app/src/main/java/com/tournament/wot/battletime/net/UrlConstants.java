package com.tournament.wot.battletime.net;

final class UrlConstants {

    private static final String BASE_WG_SERVER_URL = "https://api.worldoftanks.ru/wot/";
    static final String AUTH_URL = BASE_WG_SERVER_URL + "auth/login/";
    static final String PLAYER_INFO = BASE_WG_SERVER_URL + "account/info/";
    static final String TANKS_STATS = BASE_WG_SERVER_URL + "tanks/stats/";

//    private static final String BASE_BT_SERVER = "http://703a3909de5540f19b7867dcb1f0d3d6.cloudapp.net/WgAppService.svc/";
    private static final String BASE_BT_SERVER = "http://wgappservice.cloudapp.net/WgAppService.svc/";
    static final String BASE_SYNC = BASE_BT_SERVER + "SyncUserData";
    static final String INCREMENT_SYNC = BASE_BT_SERVER + "IncrementSync";
    static final String CREATE_SIMPLE_BATTLE = BASE_BT_SERVER + "CreateEvent";
    static final String USER_EVENT_ANSWER = BASE_BT_SERVER + "SetUserAnswer";
    static final String SIMPLE_BATTLE_UPDATE = BASE_BT_SERVER + "UpdateEvent";
    static final String GET_BEST_TANKS = BASE_BT_SERVER + "GetBestTanks";
    static final String CHANGE_OWNER_INFO = BASE_BT_SERVER + "ChangeEventOwner";
    static final String RE_SEND_INVITES = BASE_BT_SERVER + "ReSendInvites";
    static final String DELETE_INVITES = BASE_BT_SERVER + "DropEvents";
}
