package com.tournament.wot.battletime.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.core.callbacks.DialogsCallbacks;
import com.tournament.wot.battletime.ui.activitys.AuthConfirmActivity;
import com.tournament.wot.battletime.ui.dialogs.AlarmDialog;
import com.tournament.wot.battletime.ui.dialogs.FriendDialog;
import com.tournament.wot.battletime.ui.dialogs.GenericDialog;
import com.tournament.wot.battletime.ui.dialogs.TimePickerDialog;
import com.tournament.wot.battletime.ui.fragments.DrawerFragment;
import com.tournament.wot.battletime.ui.fragments.GenericFragment;
import com.tournament.wot.battletime.ui.fragments.base.ViewBaseBattleFragment;
import com.tournament.wot.battletime.ui.fragments.battle.clan.ClanBattleListFragment;
import com.tournament.wot.battletime.ui.fragments.battle.clan.CreateClanBattleFragment;
import com.tournament.wot.battletime.ui.fragments.battle.company.CompanyBattleListFragment;
import com.tournament.wot.battletime.ui.fragments.battle.company.CreateCompanyBattleFragment;
import com.tournament.wot.battletime.ui.fragments.battle.simple.CreateSimpleBattleFragment;
import com.tournament.wot.battletime.ui.fragments.battle.simple.SimpleBattleListFragment;
import com.tournament.wot.battletime.ui.fragments.battle.training.CreateTrainingBattleFragment;
import com.tournament.wot.battletime.ui.fragments.battle.training.TrainingBattleListFragment;
import com.tournament.wot.battletime.ui.fragments.splash.LoginFragment;
import com.tournament.wot.battletime.ui.fragments.tank.BestTankFragment;

public final class Launcher {

    private final FragmentManager mFragmentManager;

    public Launcher(FragmentManager pFragmentManager) {
        mFragmentManager = pFragmentManager;
    }

    private void launchFragment(GenericFragment pFragment, String pTag, boolean pBulletAnimation, boolean pCardFlipAnimation) {
        final FragmentTransaction transaction = mFragmentManager.beginTransaction();

        if (!TextUtils.isEmpty(pTag)) {
            transaction.addToBackStack(pTag);
        }
        if (pBulletAnimation) {
            transaction.setCustomAnimations(R.animator.shell_in_animation, R.animator.shell_out_animation);
        }
        if(pCardFlipAnimation){
            transaction.setCustomAnimations(
                    R.animator.card_flip_right_in, R.animator.card_flip_right_out,
                    R.animator.card_flip_left_in, R.animator.card_flip_left_out);

        }
        transaction.replace(R.id.fragment_nest_main, pFragment);
        transaction.commitAllowingStateLoss();
    }


    private void launchBulletAnimation(GenericFragment pFragment, String pTag){
        launchFragment(pFragment, pTag, true, false);
    }

    private void launchCardFlipAnimation(GenericFragment pFragment, String pTag){
        launchFragment(pFragment, pTag, false, true);
    }

    private void launchNoAnimation(GenericFragment pFragment, String pTag){
        launchFragment(pFragment, pTag, false, false);
    }


    private void launchDialog(GenericDialog pGenericDialog, String pTag) {
        pGenericDialog.show(mFragmentManager, pTag);
    }


    public void launchLoginFragment() {
        GenericFragment genericFragment = new LoginFragment();
//        launchFragment(genericFragment, null, false, false);
        launchNoAnimation(genericFragment,null);
    }


    public void launchDrawerFragment() {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.drawer_fragment_nest, new DrawerFragment());
        fragmentTransaction.commitAllowingStateLoss();
    }


    public void launchSimpleBattleListFragment() {
        GenericFragment genericFragment = new SimpleBattleListFragment();
        launchBulletAnimation(genericFragment, null);
//        launchFragment(genericFragment, null);
    }


    public void launchCreateSimplePlanFragment() {
        String tag = CreateSimpleBattleFragment.class.getSimpleName();
        GenericFragment genericFragment = new CreateSimpleBattleFragment();
        launchCardFlipAnimation(genericFragment, tag);
//        launchFragment(genericFragment, tag);
    }


    public void launchCreateSimpleCompanyFragment() {
        String tag = CreateCompanyBattleFragment.class.getSimpleName();
        GenericFragment genericFragment = new CreateCompanyBattleFragment();
        launchCardFlipAnimation(genericFragment, tag);
//        launchFragment(genericFragment, tag);
    }

    public void launchCreateClanEventFragment(Bundle pArgs) {
        String tag = CreateClanBattleFragment.class.getSimpleName();
        GenericFragment genericFragment = new CreateClanBattleFragment();
        genericFragment.setArguments(pArgs);
        launchCardFlipAnimation(genericFragment, tag);
//        launchFragment(genericFragment, tag);
    }


    public void launchViewBasicFragment(Bundle pArgs) {
        String tag = ViewBaseBattleFragment.class.getSimpleName();
        GenericFragment genericFragment = new ViewBaseBattleFragment();
        genericFragment.setArguments(pArgs);
        launchCardFlipAnimation(genericFragment,tag);
//        launchFragment(genericFragment, tag);
    }

    public void launchCompanyBattleListFragment() {
        GenericFragment genericFragment = new CompanyBattleListFragment();
        launchBulletAnimation(genericFragment, null);
//        launchFragment(genericFragment, null);
    }


    public void launchClanBattleListFragment() {
        GenericFragment genericFragment = new ClanBattleListFragment();
        launchBulletAnimation(genericFragment, null);
//        launchFragment(genericFragment, null);
    }

    public void launchTrainingBattleFragment() {
        GenericFragment genericFragment = new TrainingBattleListFragment();
        launchBulletAnimation(genericFragment, null);
//        launchFragment(genericFragment, null);
    }


    public void launchCreateTrainingBattleFragment() {
        String tag = CreateTrainingBattleFragment.class.getSimpleName();
        GenericFragment genericFragment = new CreateTrainingBattleFragment();
        launchCardFlipAnimation(genericFragment, tag);
//        launchFragment(genericFragment, tag);
    }


    public void launchBestTankFragment() {
        GenericFragment genericFragment = new BestTankFragment();
        launchBulletAnimation(genericFragment, null);
//        launchFragment(genericFragment, null);
    }

    public void launchAutConfirmActivity(Fragment pStartTableFragment, String pAuthUrl) {
        Intent intent = AuthConfirmActivity.buildIntent(pStartTableFragment.getActivity(), pAuthUrl);
        pStartTableFragment.startActivityForResult(intent, AuthConfirmActivity.THIS_ACTIVITY_START_KEY);
    }


    public void launchFriendsDialog(DialogsCallbacks pDialogsCallbacks, Bundle pArguments) {
        FriendDialog genericDialog = new FriendDialog();
        genericDialog.setArguments(pArguments);
        genericDialog.setDialogsCallbacks(pDialogsCallbacks);
        launchDialog(genericDialog, FriendDialog.class.getName());
    }


    public void launchTimeDialog(DialogsCallbacks pDialogsCallbacks) {
        GenericDialog genericDialog = new TimePickerDialog();
        genericDialog.setDialogsCallbacks(pDialogsCallbacks);
        launchDialog(genericDialog, TimePickerDialog.class.getSimpleName());
    }

    public void launchAlarmDialog(DialogsCallbacks pDialogsCallbacks, Bundle pArguments) {
        GenericDialog genericDialog = new AlarmDialog();
        genericDialog.setDialogsCallbacks(pDialogsCallbacks);
        genericDialog.setArguments(pArguments);
        launchDialog(genericDialog, AlarmDialog.class.getSimpleName());
    }
}
