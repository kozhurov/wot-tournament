package com.tournament.wot.battletime.ui.fragments.battle.simple;

import android.app.Activity;

import com.tournament.wot.battletime.model.battle.BattleModel;
import com.tournament.wot.battletime.ui.fragments.base.GenericListFragment;

public final class SimpleBattleListFragment extends GenericListFragment {

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setEventType(BattleModel.EventType.PLATOON);
    }
}
