package com.tournament.wot.battletime.core.alarm;


import android.content.Context;

import com.tournament.wot.battletime.core.receivers.AlarmReceiver;
import com.tournament.wot.battletime.model.alarm.AlarmModel;
import com.tournament.wot.battletime.model.battle.BattleModel;
import com.tournament.wot.battletime.restore.RestoreManager;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by bogdan on 27.10.14.
 */
public class LocalAlarmManager {

    private RestoreManager restoreManager;
    private BattleModel battleModel;
    private Context context;

    private LocalAlarmManager(){}

    public LocalAlarmManager(RestoreManager restoreManager) {
        this.restoreManager = restoreManager;

    }

    public void setAlarmForEvent(BattleModel alarmForEvent ,Context context){
        battleModel = alarmForEvent;
        this.context = context;
        if(!isCharged(alarmForEvent.getUUID(), null)){
            setAlarmAndSave(alarmForEvent.getUUID(), alarmForEvent.getRepeatTypeExtras());
        }
    }

    private boolean isCharged(String eventUIID, Collection<Integer> arrayList){
        ArrayList<AlarmModel> alarmModels = (ArrayList) restoreManager.getAlarmsByUUID(eventUIID);
        if(alarmModels.size() > 0){
            return true;
        }else{
            return false;
        }
    }

    private void setAlarmAndSave(String eventUIID, Collection<Integer> arrayList){
        AlarmModel alarmModel = null;
        ArrayList<AlarmModel> alarmModels = null;
         if(arrayList.size() > 0){
            alarmModels = new ArrayList<AlarmModel>(arrayList.size());
            for(Integer integer : arrayList){
                alarmModel = new AlarmModel();
                alarmModel.setmUUID(eventUIID);
                alarmModel.setmDayType(integer);
                alarmModels.add(alarmModel);
            }

            restoreManager.setAlarms(alarmModels);


         }else{
            alarmModel = new AlarmModel();
            alarmModel.setmDayType(-1);
            alarmModel.setmUUID(eventUIID);
            restoreManager.setAlarm(alarmModel);
         }

        alarmModels = (ArrayList) restoreManager.getAlarmsByUUID(eventUIID);

        AlarmReceiver.setAlarmNew(context,battleModel.getStartBattleDate().getTime(), battleModel.getId(),  alarmModels, battleModel.getRepeatTypeExtras());
    }
}
