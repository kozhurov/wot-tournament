package com.tournament.wot.battletime.ui.fragments.battle.clan;

import android.app.Activity;

import com.tournament.wot.battletime.model.battle.BattleModel;
import com.tournament.wot.battletime.ui.fragments.base.GenericListFragment;

public final class ClanBattleListFragment extends GenericListFragment {

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setEventType(BattleModel.EventType.CLAN);
    }
}

