package com.tournament.wot.battletime.ui.fragments.battle.company;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;

import com.tournament.wot.battletime.R;
import com.tournament.wot.battletime.model.battle.BattleModel.EventType;
import com.tournament.wot.battletime.ui.fragments.base.GenericEventFragment;

public class CreateCompanyBattleFragment extends GenericEventFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setEventType(EventType.COMPANY);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.ceate_simple_battle, menu);
    }
}
