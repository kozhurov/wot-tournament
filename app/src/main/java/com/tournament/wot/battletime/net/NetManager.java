package com.tournament.wot.battletime.net;

import android.text.TextUtils;
import android.util.Log;

import com.tournament.wot.battletime.core.Constants;
import com.tournament.wot.battletime.core.callbacks.NetCallbacks;
import com.tournament.wot.battletime.model.battle.BattleModel;
import com.tournament.wot.battletime.model.players.Friend;
import com.tournament.wot.battletime.model.players.Player;
import com.tournament.wot.battletime.model.tanks.BestTanksModel;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executor;

public final class NetManager {

    private final Executor mExecutor;

    public NetManager(Executor pExecutor) {
        mExecutor = pExecutor;
    }

    public void makeAuthorisation(final NetCallbacks pNetCallbacks) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                HttpPost httpPost = new HttpPost(UrlConstants.AUTH_URL);

                List<NameValuePair> params = new ArrayList<NameValuePair>(4);
                params.add(new BasicNameValuePair(PostParams.WG_APP_ID, ApiConstants.APP_ID));
                params.add(new BasicNameValuePair(PostParams.WG_LANGUAGE, ApiConstants.LANGUAGE_TYPE_RU));
                params.add(new BasicNameValuePair(PostParams.WG_NO_FOLLOW, String.valueOf(Constants.ONE_NUMBER_KEY)));
                params.add(new BasicNameValuePair(PostParams.WG_DISPLAY_TYPE, ApiConstants.DISPLAY_TYPE_DIALOG));

                try {
                    httpPost.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                HttpResponse response;

                try {
                    response = new DefaultHttpClient().execute(httpPost);
                } catch (IOException e) {
                    e.printStackTrace();
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    return;
                }

                if (response == null) {
                    pNetCallbacks.taskCompleteWithError("null response");
                    return;
                }

                StatusLine statusLine = response.getStatusLine();
                switch (statusLine.getStatusCode()) {
                    case HttpStatus.SC_OK:
                        HttpEntity entity = response.getEntity();
                        if (entity == null) {
                            pNetCallbacks.taskCompleteWithError("empty entity");
                            return;
                        }

                        JSONObject jsonObject = null;
                        try {
                            String stringEntity = EntityUtils.toString(entity);
                            jsonObject = new JSONObject(stringEntity);
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        try {
                            String location = jsonObject.getJSONObject(ResponseConstants.WG_DATA_OBJECT_KEY).getString(ResponseConstants.LOCATION_KEY);
                            pNetCallbacks.authComplete(location);
                        } catch (JSONException e) {
                            Log.e("NM", "error = " + e.getMessage());
                        }
                        break;
                }
            }
        });
    }

    public void loadMyInfo(final NetCallbacks pNetCallbacks, final long pAccountId, final String pAccessToken) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                Player player = null;

                HttpPost httpPost = new HttpPost(UrlConstants.PLAYER_INFO);

                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair(PostParams.WG_APP_ID, ApiConstants.APP_ID));
                params.add(new BasicNameValuePair(PostParams.WG_FIELDS, ApiConstants.PLAYER_FIELDS_STRING));
                params.add(new BasicNameValuePair(PostParams.WG_ACCOUNT_ID, String.valueOf(pAccountId)));
                params.add(new BasicNameValuePair(PostParams.WG_ACCESS_TOKEN, pAccessToken));

                try {
                    httpPost.setEntity(new UrlEncodedFormEntity(params));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    return;
                }

                HttpResponse response;
                try {
                    response = new DefaultHttpClient().execute(httpPost);
                } catch (IOException e) {
                    e.printStackTrace();
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    return;
                }

                StatusLine statusLine = response.getStatusLine();

                switch (statusLine.getStatusCode()) {
                    case HttpStatus.SC_OK:
                        HttpEntity entity = response.getEntity();
                        if (entity == null) {
                            pNetCallbacks.taskCompleteWithError("null entity");
                            return;
                        }

                        JSONObject profile;
                        try {
                            String stringEntity = EntityUtils.toString(entity);
                            JSONObject jsonObject = new JSONObject(stringEntity);
                            JSONObject dataObject = jsonObject.getJSONObject(ResponseConstants.WG_DATA_OBJECT_KEY);
                            profile = dataObject.getJSONObject(String.valueOf(pAccountId));
                        } catch (IOException e) {
                            e.printStackTrace();
                            pNetCallbacks.taskCompleteWithError(e.getMessage());
                            return;

                        } catch (JSONException e) {
                            e.printStackTrace();
                            pNetCallbacks.taskCompleteWithError(e.getMessage());
                            return;
                        }

                        if (profile == null) {
                            pNetCallbacks.taskCompleteWithError("null profile");
                            return;

                        } else {
                            try {
                                player = new Player(profile);
                                player.setId(pAccountId);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                pNetCallbacks.taskCompleteWithError(e.getMessage());
                                return;
                            }
                        }

                        break;

                    default:
                        pNetCallbacks.taskCompleteWithError(String.valueOf(statusLine.getStatusCode()));
                        return;
                }

                // Get Tank list in garage

                httpPost = new HttpPost(UrlConstants.TANKS_STATS);

                params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair(PostParams.WG_APP_ID, ApiConstants.APP_ID));
                params.add(new BasicNameValuePair(PostParams.WG_FIELDS, ApiConstants.TANK_FIELD));
                params.add(new BasicNameValuePair(PostParams.WG_ACCOUNT_ID, String.valueOf(pAccountId)));
                params.add(new BasicNameValuePair(PostParams.WG_ACCESS_TOKEN, pAccessToken));
                params.add(new BasicNameValuePair(PostParams.WG_GRAGE, ApiConstants.IN_GARAGE));

                try {
                    httpPost.setEntity(new UrlEncodedFormEntity(params));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    return;
                }

                try {
                    response = new DefaultHttpClient().execute(httpPost);
                } catch (IOException e) {
                    e.printStackTrace();
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    return;
                }

                statusLine = response.getStatusLine();


                switch (statusLine.getStatusCode()) {
                    case HttpStatus.SC_OK:
                        HttpEntity entity = response.getEntity();
                        if (entity == null) {
                            pNetCallbacks.taskCompleteWithError("null entity");
                            return;
                        }

                        JSONArray profile;
                        try {
                            String stringEntity = EntityUtils.toString(entity);

                            Log.e("TAG", stringEntity);

                            JSONObject jsonObject = new JSONObject(stringEntity);
                            JSONObject dataObject = jsonObject.getJSONObject(ResponseConstants.WG_DATA_OBJECT_KEY);
                            profile = dataObject.getJSONArray(String.valueOf(pAccountId));

                            int arraySize = profile.length();
                            int[] idsArray = new int[arraySize];
                            for (int i = 0; i < arraySize; i++) {
                                jsonObject = profile.getJSONObject(i);
                                idsArray[i] = jsonObject.getInt(ApiConstants.TANK_FIELD);

                            }
                            player.setTankArray(Arrays.toString(idsArray));
                            Log.e("TAG", Arrays.toString(idsArray));

                        } catch (IOException e) {
                            e.printStackTrace();
                            pNetCallbacks.taskCompleteWithError(e.getMessage());
                            return;

                        } catch (JSONException e) {
                            e.printStackTrace();
                            pNetCallbacks.taskCompleteWithError(e.getMessage());
                            return;
                        }

                        if (profile == null) {
                            pNetCallbacks.taskCompleteWithError("null tanks");
                            pNetCallbacks.loadMyInfoCompeted(player);
                            return;

                        } else {
                            pNetCallbacks.loadMyInfoCompeted(player);
                        }

                        break;

                    default:
                        pNetCallbacks.taskCompleteWithError(String.valueOf(statusLine.getStatusCode()));
                        break;
                }
            }
        });
    }

    public void baseSyncProcedure(final NetCallbacks pNetCallbacks, final Player pPlayer, final int pTimestamp, final String pDeviceId) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                JSONObject data = new JSONObject();
                JSONObject params = new JSONObject();
                try {
                    params.put(PostParams.BT_USER_ID, pPlayer.getId());
                    params.put(PostParams.BT_FRIENDS_IDS, new JSONArray(pPlayer.getFriendsIdCollection()));
                    params.put(PostParams.BT_TIMESTAMP, pTimestamp);
                    params.put(PostParams.BT_DEVICE_ID, pDeviceId);
                    params.put(PostParams.BT_CLAN_ID, pPlayer.getClanId());

                    String tanksIds = pPlayer.getTankArray();

                    tanksIds = tanksIds.substring(2, tanksIds.length());
                    tanksIds = tanksIds.substring(0, tanksIds.length() - 1);
                    String[] arrStrings = tanksIds.split(",");

                    JSONArray jsonArray = new JSONArray();

                    for (String lol : arrStrings) {
                        jsonArray.put(Integer.valueOf(lol.trim()));
                    }

                    params.put(PostParams.BT_TANKS_IDS, jsonArray);
                    data.put(PostParams.BT_DATA_ANSWER, params);
                    Log.e("baseSyncProcedure", data.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    return;
                }

                HttpEntity httpEntity = buildAndExecute(pNetCallbacks, new HttpPost(UrlConstants.BASE_SYNC), data);

                if (httpEntity == null) {
                    return;
                }

                try {
                    String stringEntity = EntityUtils.toString(httpEntity);
                    JSONObject jsonResponse = new JSONObject(stringEntity).getJSONObject(ResponseConstants.BT_RESULT_BASE_SYNC_DATA_OBJECT_KEY);
                    Log.e("baseSyncProcedure", jsonResponse.toString());
                    JSONArray jsonFriends = jsonResponse.getJSONArray(ResponseConstants.BASE_SYNC_FRIENDS_ARRAY);
                    int responseSize = jsonFriends.length();

                    List<Friend> friendList;
                    if (responseSize > 0) {
                        friendList = new ArrayList<Friend>(responseSize);
                        JSONObject jsonFriend;
                        for (int i = 0; i < responseSize; i++) {
                            jsonFriend = jsonFriends.getJSONObject(i);
                            friendList.add(new Friend(jsonFriend, true));
                        }
                    } else {
                        friendList = new ArrayList<Friend>();
                    }

                    JSONObject jsonObject = jsonResponse.getJSONObject(ResponseConstants.BASE_SYNC_USER_OBJECT);
                    pPlayer.extractExtraRateInfo(jsonObject);

                    pNetCallbacks.baseSyncComplete(friendList, jsonResponse.getInt(ResponseConstants.UNIVERSAL_TIMESTAMP), pPlayer);
                } catch (IOException e) {
                    e.printStackTrace();
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    return;
                }

            }
        });
    }

    public void incrementSyncProcedure(final NetCallbacks pNetCallbacks, final long pAccountId, final int pTimestamp) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {

                JSONObject data = new JSONObject();
                JSONObject params = new JSONObject();
                try {
                    params.put(PostParams.BT_TIMESTAMP, pTimestamp);
                    params.put(PostParams.BT_USER_ID, pAccountId);
                    data.put(PostParams.BT_DATA_ANSWER, params);

                    Log.e("incremSyncProcedure", data.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    return;
                }

                HttpEntity httpEntity = buildAndExecute(pNetCallbacks, new HttpPost(UrlConstants.INCREMENT_SYNC), data);

                if (httpEntity == null) {
                    return;
                }


                try {
                    String stringEntity = EntityUtils.toString(httpEntity);
                    Log.e("NM", "increm = " + stringEntity);

                    JSONObject responseData = new JSONObject(stringEntity).getJSONObject(ResponseConstants.BT_RESULT_INCREMENT_SYNC_DATA_OBJECT_KEY);

                    JSONArray clanMembers = responseData.optJSONArray(ResponseConstants.BT_DATA_CLAN_MEMBERS);
                    JSONArray clanEvents = responseData.optJSONArray(ResponseConstants.BT_DATA_CLAN_EVENTS);
                    JSONArray companyEvents = responseData.optJSONArray(ResponseConstants.BT_DATA_COMPANY_EVENTS);
                    JSONArray platoonEvents = responseData.optJSONArray(ResponseConstants.BT_DATA_PLATOONS_EVENTS);
                    JSONArray trainingEvents = responseData.optJSONArray(ResponseConstants.BT_DATA_TRAINING_EVENTS);

                    JSONObject arrayElement;

                    List<Friend> allClanMember = null;
                    if (clanMembers != null && clanMembers.length() > 0) {
                        Friend friend;
                        allClanMember = new ArrayList<Friend>(clanMembers.length());
                        for (int i = 0; i < clanMembers.length(); i++) {
                            arrayElement = clanMembers.getJSONObject(i);
                            friend = new Friend(arrayElement, true, null);
                            if (friend.getId() != pAccountId) {
                                allClanMember.add(friend);
                            }
                        }
                    }


                    BattleModel battleModel;
                    List<BattleModel> newBattles = new ArrayList<BattleModel>();

                    Log.e("incremSyncProcedure ", "clan size " + clanEvents.length());

                    if (clanEvents != null && clanEvents.length() > 0) {
                        for (int i = 0; i < clanEvents.length(); i++) {
                            arrayElement = clanEvents.getJSONObject(i);
                            battleModel = new BattleModel(arrayElement, pAccountId);
                            battleModel.setEventType(BattleModel.EventType.CLAN.ordinal());
                            newBattles.add(battleModel);
                        }
                    }

                    Log.e("incremSyncProcedure ", "company size " + companyEvents.length());

                    if (companyEvents != null && companyEvents.length() > 0) {
                        for (int i = 0; i < companyEvents.length(); i++) {
                            arrayElement = companyEvents.getJSONObject(i);
                            battleModel = new BattleModel(arrayElement, pAccountId);
                            battleModel.setEventType(BattleModel.EventType.COMPANY.ordinal());
                            newBattles.add(battleModel);
                        }
                    }

                    Log.e("incremSyncProcedure ", "platoon size " + platoonEvents.length());

                    if (platoonEvents != null && platoonEvents.length() > 0) {
                        for (int i = 0; i < platoonEvents.length(); i++) {
                            arrayElement = platoonEvents.getJSONObject(i);
                            battleModel = new BattleModel(arrayElement, pAccountId);
                            battleModel.setEventType(BattleModel.EventType.PLATOON.ordinal());
                            newBattles.add(battleModel);
                        }
                    }

                    Log.e("incremSyncProcedure ", "training size " + trainingEvents.length());

                    if (trainingEvents != null && trainingEvents.length() > 0) {
                        for (int i = 0; i < trainingEvents.length(); i++) {
                            arrayElement = trainingEvents.getJSONObject(i);
                            battleModel = new BattleModel(arrayElement, pAccountId);
                            battleModel.setEventType(BattleModel.EventType.TRAINING.ordinal());
                            newBattles.add(battleModel);
                        }
                    }

                    int timestamp = responseData.getInt(ResponseConstants.UNIVERSAL_TIMESTAMP);

                    pNetCallbacks.incrementSyncComplete(timestamp, newBattles, allClanMember);

                } catch (IOException e) {
                    e.printStackTrace();
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    return;
                } catch (JSONException e) {
                    e.printStackTrace();
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    return;
                } catch (ParseException e) {
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    public void userAnswer(final NetCallbacks pNetCallbacks, final long pAccountId, final String pEventUUID, final int pUserAnswerFlag) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {

                JSONObject data = new JSONObject();
                JSONObject params = new JSONObject();
                try {
                    params.put(PostParams.BT_EVENT_ID, pEventUUID);
                    params.put(PostParams.BT_USER_ID, pAccountId);
                    params.put(PostParams.BT_EVENT_ANSWER, pUserAnswerFlag);
                    data.put(PostParams.BT_DATA_ANSWER, params);
                } catch (JSONException e) {
                    e.printStackTrace();
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    return;
                }

                HttpEntity httpEntity = buildAndExecute(pNetCallbacks, new HttpPost(UrlConstants.USER_EVENT_ANSWER), data);

                if (httpEntity == null) {
                    return;
                }

                String stringEntity = null;
                try {
                    stringEntity = EntityUtils.toString(httpEntity);
                    JSONObject jsonResponse = new JSONObject(stringEntity).getJSONObject(ResponseConstants.BT_USER_ANSWER_RESULT);
                    pNetCallbacks.userAnswerAccepted(jsonResponse.getInt(ResponseConstants.UNIVERSAL_TIMESTAMP), pUserAnswerFlag);

                } catch (IOException e) {
                    e.printStackTrace();
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    return;

                } catch (JSONException e) {
                    e.printStackTrace();
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    return;
                }

            }
        });
    }

    public void reSendInvites(final NetCallbacks pNetCallbacks, final String pEventUUID) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {

                JSONObject data = new JSONObject();
                JSONObject params = new JSONObject();
                try {
                    params.put(PostParams.BT_EVENT_ID, pEventUUID);
                    data.put(PostParams.BT_DATA_ANSWER, params);
                } catch (JSONException e) {
                    e.printStackTrace();
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    return;
                }

                HttpEntity httpEntity = buildAndExecute(pNetCallbacks, new HttpPost(UrlConstants.RE_SEND_INVITES), data);

                if (httpEntity == null) {
                    return;
                }

                pNetCallbacks.reSendComplete();

            }
        });
    }

    public void commanderSelected(NetCallbacks pNetCallbacks, String pEventUUID, long pSelectedCommanderId) {

    }

    public void createBattleModel(final NetCallbacks pNetCallbacks, final BattleModel pBattleModel) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                JSONObject data = new JSONObject();
                JSONObject params = new JSONObject();
                try {
                    params.put(PostParams.BT_EVENT_NAME, pBattleModel.getName());
                    params.put(PostParams.BT_EVENT_AUTHOR_ID, pBattleModel.getAuthorId());
                    params.put(PostParams.BT_EVENT_PARTICIPANTS_ARRAY, pBattleModel.getParticipantsInJson());

                    params.put(PostParams.BT_EVENT_START_DATE, pBattleModel.getStartBattleDateString());
                    params.put(PostParams.BT_EVENT_END_DATE, pBattleModel.getEndBattleDateString());

                    params.put(PostParams.BT_EVENT_REPEAT_TYPE, pBattleModel.getRepeatType());
                    params.put(PostParams.BT_EVENT_TYPE, pBattleModel.getEventType());

                    // TODO remove after fix

                    params.put(PostParams.BT_EVENT_EXTRA_REPEAT, new JSONArray(pBattleModel.getRepeatTypeExtras()));
                    data.put(PostParams.BT_DATA_ANSWER, params);
                    Log.e("createBattleModel", data.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    return;
                }

                HttpEntity httpEntity = buildAndExecute(pNetCallbacks, new HttpPost(UrlConstants.CREATE_SIMPLE_BATTLE), data);

                if (httpEntity == null) {
                    return;
                }

                try {
                    String stringEntity = EntityUtils.toString(httpEntity);

                    Log.e("createBattleModel", stringEntity);

                    JSONObject jsonResponse = new JSONObject(stringEntity).getJSONObject(ResponseConstants.BT_RESULT_SIMPLE_BATTLE_CREATE_KEY);
                    pBattleModel.extractServerInformation(jsonResponse);
                } catch (IOException e) {
                    e.printStackTrace();
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    return;

                } catch (JSONException e) {
                    e.printStackTrace();
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    return;
                }

                pNetCallbacks.battleCreated(pBattleModel);
            }
        });
    }

    public void updateBattleModel(final NetCallbacks pNetCallbacks, final BattleModel pIncrementBattleModel, final long pAccountId) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                JSONObject data = new JSONObject();
                JSONObject params = new JSONObject();
                try {
                    params.put(PostParams.BT_EVENT_ID, pIncrementBattleModel.getUUID());
                    params.put(PostParams.BT_USER_ID, pAccountId);
                    params.put(PostParams.BT_EVENT_NAME, pIncrementBattleModel.getName());
                    params.put(PostParams.BT_EVENT_AUTHOR_ID, pIncrementBattleModel.getAuthorId());
                    params.put(PostParams.BT_EVENT_PARTICIPANTS_ARRAY, pIncrementBattleModel.getParticipantsInJson());
                    params.put(PostParams.BT_EVENT_START_DATE, pIncrementBattleModel.getStartBattleDateString());
                    params.put(PostParams.BT_EVENT_END_DATE, pIncrementBattleModel.getEndBattleDateString());
                    params.put(PostParams.BT_EVENT_REPEAT_TYPE, pIncrementBattleModel.getRepeatType());
                    params.put(PostParams.BT_EVENT_TYPE, pIncrementBattleModel.getEventType());

                    data.put(PostParams.BT_DATA_ANSWER, params);

                    Log.e("updatem mBattleModel", data.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    return;
                }

                HttpEntity httpEntity = buildAndExecute(pNetCallbacks, new HttpPost(UrlConstants.SIMPLE_BATTLE_UPDATE), data);

                if (httpEntity == null) {
                    return;
                }

                try {
                    String stringEntity = EntityUtils.toString(httpEntity);
                    Log.e("update clan event", stringEntity);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                pNetCallbacks.battleUpdated(pIncrementBattleModel);
            }
        });
    }

    public void deleteEvent(final NetCallbacks pNetCallBacks, final String pEventUUID, final int pAccountId) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                JSONObject data = new JSONObject();
                JSONObject params = new JSONObject();

                //TODO delete signature
                try {
                    if (TextUtils.isEmpty(pEventUUID)) {
                        params.put(PostParams.BT_USER_ID, pAccountId);
                    } else {
                        params.put(PostParams.BT_EVENT_ID, pEventUUID);
                    }

                    data.put(PostParams.BT_DATA_ANSWER, params);
                    HttpEntity httpEntity = buildAndExecute(pNetCallBacks, new HttpPost(UrlConstants.DELETE_INVITES), data);

                    if (httpEntity == null) {
                        return;
                    }

                    try {
                        String stringEntity = EntityUtils.toString(httpEntity);
                        Log.e("delete event event", stringEntity);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    pNetCallBacks.deleteEventComplete();
                } catch (JSONException e) {
                    e.printStackTrace();
                    pNetCallBacks.taskCompleteWithError(e.getMessage());
                    return;
                }
            }
        });
    }

    public void loadBestTanks(final NetCallbacks pNetCallbacks, final long pAccountId, final int pRegion) {
        mExecutor.execute(new Runnable() {

            @Override
            public void run() {

                JSONObject data = new JSONObject();
                JSONObject params = new JSONObject();

                try {
                    params.put(PostParams.BT_EVENT_AUTHOR_ID, pAccountId);
                    params.put(PostParams.WG_REGION, pRegion);
                    data.put(PostParams.BT_DATA_ANSWER, params);
                    Log.e("loadBestTanks", data.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    return;
                }

                HttpEntity httpEntity = buildAndExecute(pNetCallbacks, new HttpPost(UrlConstants.GET_BEST_TANKS), data);

                if (httpEntity == null) {
                    return;
                }

                ArrayList<BestTanksModel> bestTanksModels;
                try {
                    String stringEntity = EntityUtils.toString(httpEntity);
                    Log.e("BestTanks", "best tank = " + stringEntity);

                    JSONObject jsonResponse = new JSONObject(stringEntity).getJSONObject(ResponseConstants.BT_RESULT_BEST_TANKS_KEY);
                    JSONArray jsonArray = jsonResponse.getJSONArray(ResponseConstants.BT_DATA_BEST);

                    int jsonArraySize = jsonArray.length();
                    bestTanksModels = new ArrayList<BestTanksModel>(jsonArraySize);
                    for (int i = 0; i < jsonArraySize; i++) {
                        bestTanksModels.add(new BestTanksModel(jsonArray.getJSONObject(i)));
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    return;

                } catch (JSONException e) {
                    e.printStackTrace();
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    return;
                }

                pNetCallbacks.loadBestTanks(bestTanksModels);
            }
        });
    }

    public void changeOwner(final NetCallbacks pNetCallbacks, final String pEventId, final long pNewCommanderId) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                JSONObject data = new JSONObject();
                JSONObject params = new JSONObject();

                try {
                    params.put(PostParams.BT_EVENT_ID, pEventId);
                    params.put(PostParams.BT_USER_ID, pNewCommanderId);
                    data.put(PostParams.BT_DATA_ANSWER, params);
                } catch (JSONException e) {
                    pNetCallbacks.taskCompleteWithError(e.getMessage());
                    e.printStackTrace();
                }
                HttpEntity httpEntity = buildAndExecute(pNetCallbacks, new HttpPost(UrlConstants.CHANGE_OWNER_INFO), data);

                if (httpEntity == null) {
                    return;
                }

                pNetCallbacks.changeCommanderComplete(pNewCommanderId, pEventId);
            }
        });
    }

    public void changeGlobalClanEventOwner(final NetCallbacks pNetCallbacks, final long pClanId, final long pNewCommanderId) {

    }

    private HttpEntity buildAndExecute(NetCallbacks pNetCallbacks, HttpPost pHttpPost, JSONObject pParams) {
        HttpEntity httpEntity = null;

        try {
            pHttpPost.setEntity(new StringEntity(pParams.toString(), HTTP.UTF_8));
            pHttpPost.addHeader(ApiConstants.CONTENT_TYPE_KEY, ApiConstants.CONTENT_TYPE_VALUE);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            pNetCallbacks.taskCompleteWithError(e.getMessage());
            return httpEntity;
        }

        HttpResponse response;
        try {
            response = new DefaultHttpClient().execute(pHttpPost);
        } catch (IOException e) {
            e.printStackTrace();
            pNetCallbacks.taskCompleteWithError(e.getMessage());
            return httpEntity;
        }

        StatusLine statusLine = response.getStatusLine();
        switch (statusLine.getStatusCode()) {
            case HttpStatus.SC_OK:
                httpEntity = response.getEntity();

                if (httpEntity == null) {
                    pNetCallbacks.taskCompleteWithError("empty entity");
                }

                break;

            default:
                pNetCallbacks.taskCompleteWithError(String.valueOf(statusLine.getStatusCode()));
                break;

        }

        return httpEntity;
    }
}
