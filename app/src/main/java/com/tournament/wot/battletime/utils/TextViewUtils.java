package com.tournament.wot.battletime.utils;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

public class TextViewUtils {

    public static void CheckView(String text, TextView view) {
        if (TextUtils.isEmpty(text)) {
            view.setVisibility(View.GONE);
        } else {
            view.setText(text);
            view.setVisibility(View.VISIBLE);
        }
    }

}
