package com.tournament.wot.battletime.core.receivers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.tournament.wot.battletime.core.WTApplication;
import com.tournament.wot.battletime.ui.activitys.MainActivity;
import com.tournament.wot.battletime.utils.PushAndNotifyUtils;

public final class PushReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if(((WTApplication) context.getApplicationContext()).isActivityStarted()) {
            translateInActivity(context, intent.getExtras());
        } else {
            PushAndNotifyUtils.showNotification(context, intent.getExtras());
        }

        PushReceiver.completeWakefulIntent(intent);
    }

    private void translateInActivity(Context pContext, Bundle pBundle) {
        Intent mainActivityIntent = new Intent(MainActivity.class.getSimpleName());
        mainActivityIntent.putExtras(pBundle);
        LocalBroadcastManager.getInstance(pContext).sendBroadcast(mainActivityIntent);
    }
}
