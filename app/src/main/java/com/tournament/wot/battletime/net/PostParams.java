package com.tournament.wot.battletime.net;

final class PostParams {

    static final String WG_APP_ID = "application_id";
    static final String WG_LANGUAGE = "language";
    static final String WG_TOKEN_LIFE_TIME = "expires_at";
    static final String WG_NO_FOLLOW = "nofollow";
    static final String WG_DISPLAY_TYPE = "display";
    static final String WG_FIELDS = "fields";
    static final String WG_ACCESS_TOKEN = "access_token";
    static final String WG_ACCOUNT_ID = "account_id";
    static final String WG_GRAGE = "in_garage";
    static final String WG_REGION = "Region";

    static final String BT_USER_ID = "UserId";
    static final String BT_FRIENDS_IDS = "FriendsIds";
    static final String BT_TIMESTAMP = "Timestamp";
    static final String BT_DEVICE_ID = "DeviceId";
    static final String BT_TANKS_IDS = "Tanks";

    static final String BT_EVENT_ID = "EventId";
    static final String BT_EVENT_AUTHOR_ID = "UserId";
    static final String BT_EVENT_NAME = "Name";
    static final String BT_EVENT_ANSWER = "Answer";
    static final String BT_DATA_ANSWER = "data";
    static final String BT_CLAN_ID = "ClanId";

    static final String BT_EVENT_PARTICIPANTS_ARRAY = "Participants";

    static final String BT_EVENT_START_DATE = "StartDate";
    static final String BT_EVENT_END_DATE = "EndDate";
    static final String BT_EVENT_REPEAT_TYPE = "RepeatType";
    static final String BT_EVENT_TYPE = "EventType";
    static final String BT_EVENT_EXTRA_REPEAT = "ExtraRepeatData";
    static final String BT_EVENT_ONLY_ACTIVE = "OnlyActive";


}
