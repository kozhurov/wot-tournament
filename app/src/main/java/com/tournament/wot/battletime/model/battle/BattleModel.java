package com.tournament.wot.battletime.model.battle;

import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.tournament.wot.battletime.core.Constants;
import com.tournament.wot.battletime.model.players.Participant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@DatabaseTable
public final class BattleModel {

    private static final String LAST_BATTLE_DATE_FORMAT = "dd:MM, hh:mm";
    private static final String UNIVERSAL_TIMESTAMP = "TimeStamp";
    private static final String REPEAT_TYPE = "RepeatType";
    private static final String EXTRA_REPEAT_TYPE = "ExtraRepeatData";
    private static final String EVENT_NAME = "Name";
    private static final String PARTICIPANTS_ARRAY = "Participants";
    private static final String BATTLE_TYPE = "BattleType";
    private static final String EDITABLE = "EnableToEdit";
    private static final String END_DATE = "EndDate";
    private static final String EVENT_ID = "EventId";
    private static final String EVENT_TYPE = "EventType";
    private static final String OWNER_OBJECT = "Owner";
    private static final String USER_ID = "UserId";
    private static final String IS_COMMANDER = "IsCommander";
    private static final String IS_INVITED = "IsInvited";

    private static final String LAST_UPDATE = "LastUpdated";
    private static final String IS_ACTIVE = "IsActive";

    private static final String START_DATA = "StartDate";

    private static final String RESULTS = "Results";
    private static final String MAP = "Map";

    @DatabaseField(generatedId = true)
    protected long mId;

    @DatabaseField(columnName = "UUID")
    protected String mUUID;

    @DatabaseField(columnName = "name")
    protected String mName;

    @DatabaseField(columnName = "author_id")
    protected long mAuthorId;

    @DatabaseField(columnName = "start_battle_date")
    protected Date mStartBattleDate;

    @DatabaseField(columnName = "start_battle_date_string")
    protected String mStartBattleDateString;

    @DatabaseField(columnName = "end_battle_date")
    protected Date mEndBattleDate;

    @DatabaseField(columnName = "end_battle_date_string")
    protected String mEndBattleDateString;

    @DatabaseField(columnName = "event_type")
    protected int mEventType;

    @DatabaseField(columnName = "is_active")
    protected boolean mIsActive;

    @DatabaseField(columnName = "editable")
    protected boolean mEditable;

    @DatabaseField(columnName = "last_update")
    protected int mLastUpdate;

    @DatabaseField(columnName = "result")
    protected String mBattleResult;

    @DatabaseField(columnName = "repeat_type")
    protected int mRepeatType;

    @DatabaseField(columnName = "is_invited")
    protected boolean mIsInvited;

    @DatabaseField(columnName = "current_user_answer", defaultValue = "3")
    protected int mUserAnswer;

    @DatabaseField(columnName = "extra_repeat")
    protected String mRepeatTypeExtras;

    protected Collection<Participant> mParticipants;
    protected JSONArray mParticipantsInJson;

    @ForeignCollectionField(eager = false)
    private Collection<Participant> participantsCollection;

    @DatabaseField(columnName = "participant_is_commander")
    private boolean mParticipantIsCommander;

    public BattleModel() {

    }

    public BattleModel(JSONObject pJSONObject, long pPlayerUUID) throws JSONException, ParseException {

        mName = pJSONObject.getString(EVENT_NAME);

        JSONObject owner = pJSONObject.getJSONObject(OWNER_OBJECT);
        mAuthorId = owner.getLong(USER_ID);
        Participant ownerModel = new Participant(owner);

        JSONArray participants = pJSONObject.optJSONArray(PARTICIPANTS_ARRAY);

        if (participants != null) {
            int participantsSize = participants.length();
            mParticipants = new ArrayList<Participant>(participantsSize);
            Participant participant;
            for (int i = 0; i < participantsSize; i++) {
                participant = new Participant(participants.getJSONObject(i));

                if (ownerModel.getParticipantId() != participant.getParticipantId()) {
                    mParticipants.add(participant);
                }

                if(pPlayerUUID == participant.getParticipantId()){
                    Log.e("BM", "my answer = "+participant.getParticipantAnswer());
                    mUserAnswer = participant.getParticipantAnswer();
                }

            }
        } else {
            mParticipants = new ArrayList<Participant>(1);
        }

        mParticipants.add(ownerModel);

        mEditable = pJSONObject.getBoolean(EDITABLE);
        mEventType = pJSONObject.getInt(EVENT_TYPE);
        mIsActive = pJSONObject.getBoolean(IS_ACTIVE);
        mLastUpdate = pJSONObject.getInt(LAST_UPDATE);

        long startBattle = pJSONObject.getLong(START_DATA) * 1000;
        mStartBattleDate = new Date(startBattle);
        mStartBattleDateString = Constants.getSimpleDateFormat().format(mStartBattleDate);

        long endBattle = pJSONObject.getLong(END_DATE) * 1000;
        mEndBattleDate = new Date(endBattle);
        mEndBattleDateString = Constants.getSimpleDateFormat().format(mEndBattleDate);

        extractServerInformation(pJSONObject);
        mRepeatType = pJSONObject.getInt(REPEAT_TYPE);

        mBattleResult = pJSONObject.optString(RESULTS);

        Collection<Integer> integers = new ArrayList<Integer>();
        JSONArray jsonArray = pJSONObject.getJSONArray(EXTRA_REPEAT_TYPE);
        int size = jsonArray.length();
        for (int i = 0; i < size; i++) {
            integers.add(jsonArray.getInt(i));
        }

        setRepeatTypeExtras(integers);

        mIsInvited = pJSONObject.optBoolean(IS_INVITED, false);
        mParticipantIsCommander = pJSONObject.optBoolean(IS_COMMANDER, false);
    }

    public String getName() {
        return mName;
    }

    public Date getStartBattleDate() {
        return mStartBattleDate;
    }

    public String getStartBattleDateString() {
        return mStartBattleDateString;
    }

    public Date getEndBattleDate() {
        return mEndBattleDate == null ? mStartBattleDate : mEndBattleDate;
    }

    public Collection<Participant> getParticipants() {
        if (mParticipants == null) {
            mParticipants = new ArrayList<Participant>();
        }
        return mParticipants;
    }

    public void setParticipants(Collection<Participant> pParticipants) {
        this.participantsCollection = pParticipants;
    }

    public long getAuthorId() {
        return mAuthorId;
    }

    public void setAuthorId(long pAuthorId) {
        mAuthorId = pAuthorId;
    }

    public String getUUID() {
        return mUUID;
    }

    public void setUUID(String pUUID) {
        mUUID = pUUID;
    }

    public JSONArray getParticipantsInJson() {
        if (mParticipantsInJson == null) {
            mParticipantsInJson = new JSONArray();
        }
        for (Participant participant : mParticipants) {
            try {
                if (participant.getParticipantId() > 0) {
                    mParticipantsInJson.put(participant.getConvertedJsonObject());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mParticipantsInJson;
    }

    public String getEndBattleDateString() {

        if (TextUtils.isEmpty(mEndBattleDateString)) {
            mEndBattleDateString = mStartBattleDateString;
        }

        return mEndBattleDateString;
    }

    public Collection<Participant> getParticipantsFromBase() {
        if (participantsCollection != null) {
            return participantsCollection;
        } else {
            return getParticipants();
        }
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        this.mId = id;
    }

    public int getEventType() {
        return mEventType;
    }

    public void setEventType(int pEventType) {
        this.mEventType = pEventType;
    }

    public boolean isActive() {
        return mIsActive;
    }

    public int getLastUpdate() {
        return mLastUpdate;
    }

    public boolean isEditable() {
        return mEditable;
    }

    public void setEditable(boolean pIsEditable) {
        mEditable = pIsEditable;
    }

    public void setBattleName(String pBattleName) {
        mName = pBattleName;
    }

    public void setStartBattleTime(Date pStartDate) {
        mStartBattleDate = pStartDate;
        mStartBattleDateString = DateFormat.format(Constants.YYYY_MM_DD_HH_MM_DATE_FORMAT, pStartDate).toString();
    }

    public void setEndBattleTime(Date pEndDate) {
        mEndBattleDate = pEndDate;
    }

    public void setBattleParticipants(Collection<Participant> pBattleParticipants) {
        mParticipants = pBattleParticipants;
    }

    public void setRepeatTypeExtras(Collection<Integer> pRepeatTypeExtras) {
        mRepeatTypeExtras = Constants.EMPTY_STRING;
        for (Integer integer : pRepeatTypeExtras) {
            mRepeatTypeExtras += integer.toString();
        }
    }

    public void extractServerInformation(JSONObject pJsonSimpleEvent) throws JSONException {
        mUUID = pJsonSimpleEvent.getString(EVENT_ID);
    }

    public int getRepeatType() {
        return mRepeatType;
    }

    public Collection<Integer> getRepeatTypeExtras() {

        if (TextUtils.isEmpty(mRepeatTypeExtras)) {
            return new ArrayList<Integer>();
        }

        char[] repeats = mRepeatTypeExtras.toCharArray();
        List<Integer> integers = new ArrayList<Integer>(repeats.length);
        for (Character c : repeats) {
            integers.add(Integer.parseInt(c.toString()));
        }

        return integers;
    }

    public void setRepeatType(int pRepeatType) {
        mRepeatType = pRepeatType;
    }

    public String getBattleResult() {
        return mBattleResult;
    }

    public boolean isInvited() {
        return mIsInvited;
    }

    public boolean isCommander() {
        return mParticipantIsCommander;
    }

    public int getUserAnswer() {
        return mUserAnswer;
    }

    public void setUserAnswer(int pUserAnswer) {
        mUserAnswer = pUserAnswer;
    }

    public enum EventType {
        PLATOON,
        COMPANY,
        CLAN,
        TRAINING
    }

    public enum AnswerType {
        YES,
        NO,
        MAYBE
    }
}
