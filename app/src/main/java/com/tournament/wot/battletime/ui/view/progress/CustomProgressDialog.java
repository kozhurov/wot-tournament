package com.tournament.wot.battletime.ui.view.progress;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.tournament.wot.battletime.R;

public class CustomProgressDialog extends ProgressDialog {

    private Animation mRotateAnimation;
    private View mProgressImage;

    public CustomProgressDialog(Context context) {
        super(context);
    }

    public CustomProgressDialog(Context context, int theme) {
        super(context, theme);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.progress_layout);

        mProgressImage = findViewById(R.id.progress_image);
        initAnimation();
    }

    @Override
    public void onStart() {
        super.onStart();
        mProgressImage.startAnimation(mRotateAnimation);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mProgressImage.clearAnimation();
    }

    private void initAnimation() {
        if (mRotateAnimation == null) {
            mRotateAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.progress_animation);

        }
    }
}
